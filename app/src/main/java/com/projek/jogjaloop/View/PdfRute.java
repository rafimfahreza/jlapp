package com.projek.jogjaloop.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.Model.MapModel;
import com.projek.jogjaloop.Model.RuteModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.RuteAdapter;
import com.projek.jogjaloop.ViewModel.TrayekAdapter;
import com.projek.jogjaloop.databinding.ActivityPdfRuteBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PdfRute extends AppCompatActivity implements RuteAdapter.ItemAdapterCallback {

    ActivityPdfRuteBinding activityPdfRuteBinding;

    List< RuteModel > ruteModelList = new ArrayList<>();
    RuteAdapter ruteAdapter;

    public static int id_trayek;
    public static String nama_trayek;
    public static String origin;
    public static String destination;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityPdfRuteBinding = ActivityPdfRuteBinding.inflate(getLayoutInflater());
        setContentView(activityPdfRuteBinding.getRoot());

        setSupportActionBar(activityPdfRuteBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        pd = new ProgressDialog(PdfRute.this);
        pd.setMessage("Menampilkan List Trayek...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        loadTrayek();

        TextView cv_warna = activityPdfRuteBinding.cvWarna;
        ImageView gambar_rute = activityPdfRuteBinding.imageView3;

        if (nama_trayek.equals("1A")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_1a);
            gambar_rute.setBackgroundResource(R.drawable.trayek_1);
        } else if (nama_trayek.equals("1B")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_1b);
            gambar_rute.setBackgroundResource(R.drawable.trayek_2);
        } else if (nama_trayek.equals("2A")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_2a);
            gambar_rute.setBackgroundResource(R.drawable.trayek_3);
        } else if (nama_trayek.equals("2B")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_2b);
            gambar_rute.setBackgroundResource(R.drawable.trayek_4);
        } else if (nama_trayek.equals("3A")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_3a);
            gambar_rute.setBackgroundResource(R.drawable.trayek_5);
        } else if (nama_trayek.equals("3B")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_3b);
            gambar_rute.setBackgroundResource(R.drawable.trayek_6);
        } else if (nama_trayek.equals("4A")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_4a);
            gambar_rute.setBackgroundResource(R.drawable.trayek_7);
        } else if (nama_trayek.equals("4B")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_4b);
            gambar_rute.setBackgroundResource(R.drawable.trayek_8);
        } else if (nama_trayek.equals("5A")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_5a);
            gambar_rute.setBackgroundResource(R.drawable.trayek_9);
        } else if (nama_trayek.equals("5B")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_5b);
            gambar_rute.setBackgroundResource(R.drawable.trayek_10);
        } else if (nama_trayek.equals("6A")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_6a);
            gambar_rute.setBackgroundResource(R.drawable.trayek_11);
        } else if (nama_trayek.equals("6B")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_6b);
            gambar_rute.setBackgroundResource(R.drawable.trayek_12);
        } else if (nama_trayek.equals("7")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_7);
            gambar_rute.setBackgroundResource(R.drawable.trayek_13);
        } else if (nama_trayek.equals("8")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_8);
            gambar_rute.setBackgroundResource(R.drawable.trayek_14);
        } else if (nama_trayek.equals("9")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_9);
            gambar_rute.setBackgroundResource(R.drawable.trayek_15);
        } else if (nama_trayek.equals("10")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_10);
            gambar_rute.setBackgroundResource(R.drawable.trayek_16);
        } else if (nama_trayek.equals("11")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_11);
            gambar_rute.setBackgroundResource(R.drawable.trayek_17);
        } else if (nama_trayek.equals("Ngemplak (PP)")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_12);
            gambar_rute.setBackgroundResource(R.drawable.trayek_18);
        } else if (nama_trayek.equals("Ngaglik (PP)")) {
            cv_warna.setBackgroundResource(R.drawable.ic_color_13);
            gambar_rute.setBackgroundResource(R.drawable.trayek_19);
        } else {
            cv_warna.setBackgroundResource(R.drawable.ic_color_14);
            gambar_rute.setBackgroundResource(R.drawable.trayek_20);
        }

        activityPdfRuteBinding.tvNamaTrayek.setText("Trayek " + nama_trayek);
        activityPdfRuteBinding.tvRuteTrayek.setText(origin + " - " + destination);

        activityPdfRuteBinding.ivZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(PdfRute.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_custom_layout, null);
                PhotoView photoView = mView.findViewById(R.id.imageView);

                if (nama_trayek.equals("1A")) {
                    photoView.setImageResource(R.drawable.trayek_1);
                } else if (nama_trayek.equals("1B")) {
                    photoView.setImageResource(R.drawable.trayek_2);
                } else if (nama_trayek.equals("2A")) {
                    photoView.setImageResource(R.drawable.trayek_3);
                } else if (nama_trayek.equals("2B")) {
                    photoView.setImageResource(R.drawable.trayek_4);
                } else if (nama_trayek.equals("3A")) {
                    photoView.setImageResource(R.drawable.trayek_5);
                } else if (nama_trayek.equals("3B")) {
                    photoView.setImageResource(R.drawable.trayek_6);
                } else if (nama_trayek.equals("4A")) {
                    photoView.setImageResource(R.drawable.trayek_7);
                } else if (nama_trayek.equals("4B")) {
                    photoView.setImageResource(R.drawable.trayek_8);
                } else if (nama_trayek.equals("5A")) {
                    photoView.setImageResource(R.drawable.trayek_9);
                } else if (nama_trayek.equals("5B")) {
                    photoView.setImageResource(R.drawable.trayek_10);
                } else if (nama_trayek.equals("6A")) {
                    photoView.setImageResource(R.drawable.trayek_11);
                } else if (nama_trayek.equals("6B")) {
                    photoView.setImageResource(R.drawable.trayek_12);
                } else if (nama_trayek.equals("7")) {
                    photoView.setImageResource(R.drawable.trayek_13);
                } else if (nama_trayek.equals("8")) {
                    photoView.setImageResource(R.drawable.trayek_14);
                } else if (nama_trayek.equals("9")) {
                    photoView.setImageResource(R.drawable.trayek_15);
                } else if (nama_trayek.equals("10")) {
                    photoView.setImageResource(R.drawable.trayek_16);
                } else if (nama_trayek.equals("11")) {
                    photoView.setImageResource(R.drawable.trayek_17);
                } else if (nama_trayek.equals("Ngemplak (PP)")) {
                    photoView.setImageResource(R.drawable.trayek_18);
                } else if (nama_trayek.equals("Ngaglik (PP)")) {
                    photoView.setImageResource(R.drawable.trayek_19);
                } else {
                    photoView.setImageResource(R.drawable.trayek_20);
                }

                mBuilder.setView(mView);
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });
    }

    private void loadTrayek() {
        new JsonTaskRoutes().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek);
    }

    private class JsonTaskRoutes extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    ruteModelList.add(new RuteModel(users.getString("name"),rutes,rutess));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                ruteAdapter = new RuteAdapter(ruteModelList, PdfRute.this);
                activityPdfRuteBinding.rvRute.setLayoutManager(new LinearLayoutManager(PdfRute.this));
                activityPdfRuteBinding.rvRute.setAdapter(ruteAdapter);

                pd.dismiss();

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }
}