package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.databinding.ActivityListTrayekBinding;
import com.projek.jogjaloop.databinding.ActivityStreetViewBinding;

public class StreetView extends AppCompatActivity {

    public static double lat, lon;
    public static String keterangan;

    private ActivityStreetViewBinding activityListTrayekBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityListTrayekBinding = ActivityStreetViewBinding.inflate(getLayoutInflater());
        setContentView(activityListTrayekBinding.getRoot());

        setSupportActionBar(activityListTrayekBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        LatLng jogja = new LatLng(lat, lon);

        SupportStreetViewPanoramaFragment streetViewPanoramaFragment =
                (SupportStreetViewPanoramaFragment)
                        getSupportFragmentManager().findFragmentById(R.id.streetviewpanorama);

        Log.d("TAG", "data= "+lat+", "+lon);

        streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                new OnStreetViewPanoramaReadyCallback() {
                    @Override
                    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                        // Only set the panorama to SYDNEY on startup (when no panoramas have been
                        // loaded which is when the savedInstanceState is null).
                        if (savedInstanceState == null) {
                            panorama.setPosition(jogja);
                        }
                    }
                });
    }
}