package com.projek.jogjaloop.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.databinding.ActivityBottomNavigationBinding;

public class BottomNavigationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityBottomNavigationBinding activityBottomNavigationBinding = ActivityBottomNavigationBinding.inflate(getLayoutInflater());
        setContentView(activityBottomNavigationBinding.getRoot());

        getSupportActionBar().hide();

        activityBottomNavigationBinding.bottomNavigation.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selected_item = null;

                    switch (menuItem.getItemId()){
                        case R.id.nav_home:
                            selected_item = new DashboardFragment();
                            break;
                        case R.id.nav_route:
                            selected_item = new RuteFragment();
                            break;
                        case  R.id.nav_jadwal:
                            selected_item = new JadwalFragment();
                            break;
                        case R.id.nav_share:
                            selected_item = new BagiFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selected_item).commit();

                    return true;
                }
            };
}