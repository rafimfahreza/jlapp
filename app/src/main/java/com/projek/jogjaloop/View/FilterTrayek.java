package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.databinding.ActivityFilterTrayekBinding;

public class FilterTrayek extends AppCompatActivity {

    private ActivityFilterTrayekBinding activityFilterTrayekBinding;
    private CheckBox cb_1a, cb_1b, cb_2a, cb_2b, cb_3a, cb_3b, cb_4a, cb_4b, cb_5a, cb_5b, cb_6a, cb_6b, cb_7, cb_8, cb_9, cb_10, cb_11, cb_12, cb_13, cb_14;

    Integer jumlah_pilih = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityFilterTrayekBinding = ActivityFilterTrayekBinding.inflate(getLayoutInflater());
        setContentView(activityFilterTrayekBinding.getRoot());

        cb_1a = activityFilterTrayekBinding.cb1a;
        cb_1b = activityFilterTrayekBinding.cb1b;
        cb_2a = activityFilterTrayekBinding.cb2a;
        cb_2b = activityFilterTrayekBinding.cb2b;
        cb_3a = activityFilterTrayekBinding.cb3a;
        cb_3b = activityFilterTrayekBinding.cb3b;
        cb_4a = activityFilterTrayekBinding.cb4a;
        cb_4b = activityFilterTrayekBinding.cb4b;
        cb_5a = activityFilterTrayekBinding.cb5a;
        cb_5b = activityFilterTrayekBinding.cb5b;
        cb_6a = activityFilterTrayekBinding.cb6a;
        cb_6b = activityFilterTrayekBinding.cb6b;
        cb_7 = activityFilterTrayekBinding.cb7;
        cb_8 = activityFilterTrayekBinding.cb8;
        cb_9 = activityFilterTrayekBinding.cb9;
        cb_10 = activityFilterTrayekBinding.cb10;
        cb_11 = activityFilterTrayekBinding.cb11;
        cb_12 = activityFilterTrayekBinding.cb12;
        cb_13 = activityFilterTrayekBinding.cb13;
        cb_14 = activityFilterTrayekBinding.cb14;

        setSupportActionBar(activityFilterTrayekBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        activityFilterTrayekBinding.btnPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cb_1a.isChecked() && !cb_1b.isChecked() && !cb_2a.isChecked() && !cb_2b.isChecked() && !cb_3a.isChecked()
                        && !cb_3b.isChecked() && !cb_4a.isChecked() && !cb_4b.isChecked() && !cb_5a.isChecked() && !cb_5b.isChecked()
                        && !cb_6a.isChecked() && !cb_6b.isChecked() && !cb_7.isChecked() && !cb_8.isChecked() && !cb_9.isChecked()
                        && !cb_10.isChecked() && !cb_11.isChecked() && !cb_12.isChecked() && !cb_13.isChecked() && !cb_14.isChecked())
                {
                    Snackbar snackbar = Snackbar.make(activityFilterTrayekBinding.layout, "Belum ada yg dipilih.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else {

                    if (cb_1a.isChecked()){
                        FilterPolyline.trayek_1a = "pilih";
                        FilterPolyline.id_trayek_1a = 1;
                        FilterPolyline.id_trayek_pilih_polyline = 1;
                        FilterPolyline.drawable_trayek_1a = R.drawable.ic_color_1a;

                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_1b.isChecked()){
                        FilterPolyline.trayek_1b = "pilih";
                        FilterPolyline.id_trayek_1b = 2;
                        FilterPolyline.id_trayek_pilih_polyline = 2;
                        FilterPolyline.drawable_trayek_1b = R.drawable.ic_color_1b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_2a.isChecked()){
                        FilterPolyline.trayek_2a = "pilih";
                        FilterPolyline.id_trayek_2a = 3;
                        FilterPolyline.id_trayek_pilih_polyline = 3;
                        FilterPolyline.drawable_trayek_2a = R.drawable.ic_color_2a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_2b.isChecked()){
                        FilterPolyline.trayek_2b = "pilih";
                        FilterPolyline.id_trayek_2b = 4;
                        FilterPolyline.id_trayek_pilih_polyline = 4;
                        FilterPolyline.drawable_trayek_2b = R.drawable.ic_color_2b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_3a.isChecked()){
                        FilterPolyline.trayek_3a = "pilih";
                        FilterPolyline.id_trayek_3a = 5;
                        FilterPolyline.id_trayek_pilih_polyline = 5;
                        FilterPolyline.drawable_trayek_3a = R.drawable.ic_color_3a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_3b.isChecked()){
                        FilterPolyline.trayek_3b = "pilih";
                        FilterPolyline.id_trayek_3b = 6;
                        FilterPolyline.id_trayek_pilih_polyline = 6;
                        FilterPolyline.drawable_trayek_3b = R.drawable.ic_color_3b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_4a.isChecked()){
                        FilterPolyline.trayek_4a = "pilih";
                        FilterPolyline.id_trayek_4a = 7;
                        FilterPolyline.id_trayek_pilih_polyline = 7;
                        FilterPolyline.drawable_trayek_4a = R.drawable.ic_color_4a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_4b.isChecked()){
                        FilterPolyline.trayek_4b = "pilih";
                        FilterPolyline.id_trayek_4b = 8;
                        FilterPolyline.id_trayek_pilih_polyline = 8;
                        FilterPolyline.drawable_trayek_4b = R.drawable.ic_color_4b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_5a.isChecked()){
                        FilterPolyline.trayek_5a = "pilih";
                        FilterPolyline.id_trayek_5a = 9;
                        FilterPolyline.id_trayek_pilih_polyline = 9;
                        FilterPolyline.drawable_trayek_5a = R.drawable.ic_color_5b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_5b.isChecked()){
                        FilterPolyline.trayek_5b = "pilih";
                        FilterPolyline.id_trayek_5b = 10;
                        FilterPolyline.id_trayek_pilih_polyline = 10;
                        FilterPolyline.drawable_trayek_5b = R.drawable.ic_color_5b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_6a.isChecked()){
                        FilterPolyline.trayek_6a = "pilih";
                        FilterPolyline.id_trayek_6a = 11;
                        FilterPolyline.id_trayek_pilih_polyline = 11;
                        FilterPolyline.drawable_trayek_6a = R.drawable.ic_color_6a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_6b.isChecked()){
                        FilterPolyline.trayek_6b = "pilih";
                        FilterPolyline.id_trayek_6b = 12;
                        FilterPolyline.id_trayek_pilih_polyline = 12;
                        FilterPolyline.drawable_trayek_6b = R.drawable.ic_color_6b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_7.isChecked()){
                        FilterPolyline.trayek_7 = "pilih";
                        FilterPolyline.id_trayek_7 = 13;
                        FilterPolyline.id_trayek_pilih_polyline = 13;
                        FilterPolyline.drawable_trayek_7 = R.drawable.ic_color_7;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_8.isChecked()){
                        FilterPolyline.trayek_8 = "pilih";
                        FilterPolyline.id_trayek_8 = 14;
                        FilterPolyline.id_trayek_pilih_polyline = 14;
                        FilterPolyline.drawable_trayek_8 = R.drawable.ic_color_8;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_9.isChecked()){
                        FilterPolyline.trayek_9 = "pilih";
                        FilterPolyline.id_trayek_9 = 15;
                        FilterPolyline.id_trayek_pilih_polyline = 15;
                        FilterPolyline.drawable_trayek_9 = R.drawable.ic_color_9;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_10.isChecked()){
                        FilterPolyline.trayek_10 = "pilih";
                        FilterPolyline.id_trayek_10 = 16;
                        FilterPolyline.id_trayek_pilih_polyline = 16;
                        FilterPolyline.drawable_trayek_10 = R.drawable.ic_color_10;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_11.isChecked()){
                        FilterPolyline.trayek_11 = "pilih";
                        FilterPolyline.id_trayek_11 = 17;
                        FilterPolyline.id_trayek_pilih_polyline = 17;
                        FilterPolyline.drawable_trayek_11 = R.drawable.ic_color_11;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_12.isChecked()){
                        FilterPolyline.trayek_12 = "pilih";
                        FilterPolyline.id_trayek_12 = 18;
                        FilterPolyline.id_trayek_pilih_polyline = 18;
                        FilterPolyline.drawable_trayek_12 = R.drawable.ic_color_12;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_13.isChecked()){
                        FilterPolyline.trayek_13 = "pilih";
                        FilterPolyline.id_trayek_13 = 19;
                        FilterPolyline.id_trayek_pilih_polyline = 19;
                        FilterPolyline.drawable_trayek_13 = R.drawable.ic_color_13;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_14.isChecked()){
                        FilterPolyline.trayek_14 = "pilih";
                        FilterPolyline.id_trayek_14 = 20;
                        FilterPolyline.id_trayek_pilih_polyline = 20;
                        FilterPolyline.drawable_trayek_14 = R.drawable.ic_color_14;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    FilterPolyline.jumlah_pilih = jumlah_pilih;
                    startActivity(new Intent(getApplicationContext(), FilterPolyline.class));
                    finish();
                }
            }
        });
    }
}