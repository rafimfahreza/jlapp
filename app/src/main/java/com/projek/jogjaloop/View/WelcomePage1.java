package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.Adapter;
import com.projek.jogjaloop.databinding.ActivityWelcomePage1Binding;

public class WelcomePage1 extends AppCompatActivity {

    ViewPager viewPager;
    ConstraintLayout btnNext;
    int[] layouts;
    Adapter adapter;
    ActivityWelcomePage1Binding activityWelcomePage1Binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityWelcomePage1Binding = ActivityWelcomePage1Binding.inflate(getLayoutInflater());
        setContentView(activityWelcomePage1Binding.getRoot());

        getSupportActionBar().hide();

        viewPager = activityWelcomePage1Binding.pager;
        btnNext = activityWelcomePage1Binding.btnLanjut;
        layouts = new int[] {
                R.layout.activity_welcome_page_1_fix,
                R.layout.activity_welcome_page2
        };

        adapter = new Adapter(this, layouts);
        viewPager.setAdapter(adapter);
        activityWelcomePage1Binding.btnLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() + 1 < layouts.length){
                    viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
                }else {
                    startActivity(new Intent(getApplicationContext(),BottomNavigationActivity.class));
                    finish();
                }
            }
        });
        viewPager.addOnPageChangeListener(viewPagerChangeListener);
    }

    ViewPager.OnPageChangeListener viewPagerChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            if(i == layouts.length - 1){
                activityWelcomePage1Binding.textView15.setText("Lanjut");
            }else {
                activityWelcomePage1Binding.textView15.setText("Masuk");
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}