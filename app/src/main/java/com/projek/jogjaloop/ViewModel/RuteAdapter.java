package com.projek.jogjaloop.ViewModel;

import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.projek.jogjaloop.Model.MapModel;
import com.projek.jogjaloop.Model.RuteModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.View.DetailListTrayek;
import com.projek.jogjaloop.View.StreetView;

import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

public class RuteAdapter extends RecyclerView.Adapter<RuteAdapter.ViewHolder> {

    private List< RuteModel > rvData;
    private RuteAdapter.ItemAdapterCallback itemAdapterCallback;

    public RuteAdapter(List< RuteModel> ruteModels, RuteAdapter.ItemAdapterCallback mItemAdapterCallback) {
        rvData = ruteModels;
        itemAdapterCallback = mItemAdapterCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rute, parent, false);
        final RuteAdapter.ViewHolder viewHolder = new RuteAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(rvData.get(position));
        Integer posisi = rvData.size() - 1;
        if (position == 0){
            holder.tv_rounded.setBackgroundResource(R.drawable.tv_rounded_line_hijau);
            holder.tv_rute.setTypeface(holder.tv_rute.getTypeface(), Typeface.BOLD);
            holder.tv_rute.setTextSize((float) 14.0);
        }else if (position == posisi){
            holder.tv_rounded.setBackgroundResource(R.drawable.tv_rounded_oren);
            holder.tv_garis.setVisibility(View.INVISIBLE);
            holder.tv_rute.setTypeface(holder.tv_rute.getTypeface(), Typeface.BOLD);
            holder.tv_rute.setTextSize((float) 14.0);
        }else {
            holder.tv_rounded.setBackgroundResource(R.drawable.tv_rounded_abu);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StreetView.lon = rvData.get(position).getLon();
                StreetView.lat = rvData.get(position).getLat();
                StreetView.keterangan = "rute";
                Intent intent = new Intent(holder.itemView.getContext(), StreetView.class);
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rvData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_rute;
        public TextView tv_rounded;
        public TextView tv_garis;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_rute = itemView.findViewById(R.id.tv_rute);
            tv_rounded = itemView.findViewById(R.id.tv_rounded);
            tv_garis = itemView.findViewById(R.id.tv_garis);

        }

        void bind(final RuteModel mapModel){
            tv_rute.setText(mapModel.getNama_rute());
        }
    }

    public interface ItemAdapterCallback {
    }

    public void updateData(ArrayList<RuteModel> viewModels) {
        rvData.clear();
        rvData.addAll(viewModels);
        notifyDataSetChanged();
    }
}
