package com.projek.jogjaloop.ViewModel;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.View.DetailListTrayek;
import com.projek.jogjaloop.View.PdfRute;
import com.projek.jogjaloop.View.StreetView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class TrayekAdapter extends RecyclerView.Adapter<TrayekAdapter.ViewHolder> {

    public static String keterangan;

    private List< ListTrayekModel > rvData;
    private TrayekAdapter.ItemAdapterCallback itemAdapterCallback;

    public TrayekAdapter(List< ListTrayekModel> detailHalteModels, TrayekAdapter.ItemAdapterCallback mItemAdapterCallback) {
        rvData = detailHalteModels;
        itemAdapterCallback = mItemAdapterCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_map, parent, false);
        final TrayekAdapter.ViewHolder viewHolder = new TrayekAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(rvData.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (keterangan.equals("rute_fragment")){
                    PdfRute.id_trayek = rvData.get(position).getId();
                    PdfRute.destination = rvData.get(position).getDestination();
                    PdfRute.nama_trayek = rvData.get(position).getTrayek();
                    PdfRute.origin = rvData.get(position).getOrigin();

                    Intent intent = new Intent(holder.itemView.getContext(), PdfRute.class);
                    holder.itemView.getContext().startActivity(intent);
                }else if (keterangan.equals("rute terdekat")){

                    DetailListTrayek.id_trayek = rvData.get(position).getId();
                    DetailListTrayek.destination = rvData.get(position).getDestination();
                    DetailListTrayek.nama_trayek = rvData.get(position).getTrayek();
                    DetailListTrayek.origin = rvData.get(position).getOrigin();
                    DetailListTrayek.keterangan = "rute terdekat";
                    Log.d("TAG", "id_trayek= "+rvData.get(position).getId()+"\n"+"destination= "+rvData.get(position).getDestination()+"\n"+"nama_trayek= "+rvData.get(position).getTrayek()+"\n"+"origin = "+rvData.get(position).getOrigin());
                    Intent intent = new Intent(holder.itemView.getContext(), DetailListTrayek.class);
                    holder.itemView.getContext().startActivity(intent);
                }else {
                    DetailListTrayek.id_trayek = rvData.get(position).getId();
                    DetailListTrayek.destination = rvData.get(position).getDestination();
                    DetailListTrayek.nama_trayek = rvData.get(position).getTrayek();
                    DetailListTrayek.origin = rvData.get(position).getOrigin();
                    DetailListTrayek.keterangan = "rute 360";

                    Intent intent = new Intent(holder.itemView.getContext(), DetailListTrayek.class);
                    holder.itemView.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return rvData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_trayek;
        public TextView tv_rute;
        public TextView cv_warna;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_trayek = itemView.findViewById(R.id.trayek);
            tv_rute = itemView.findViewById(R.id.tv_rute);
            cv_warna = itemView.findViewById(R.id.cv_warna);

        }

        void bind(final ListTrayekModel listTrayekModel){

            if (listTrayekModel.getTrayek().equals("Ngemplak (PP)")){
                tv_trayek.setText("12");
            }else if (listTrayekModel.getTrayek().equals("Ngaglik (PP)")){
                tv_trayek.setText("13");
            }else if (listTrayekModel.getTrayek().equals("Ngabean (PP)")){
                tv_trayek.setText("14");
            }else {
                tv_trayek.setText(listTrayekModel.getTrayek());
            }

            if (keterangan.equals("rute_fragment")){

                tv_rute.setText(listTrayekModel.getOrigin() + " - "+ listTrayekModel.getDestination());

                if (listTrayekModel.getTrayek().equals("1A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_1a);
                }else if (tv_trayek.getText().equals("1B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_1b);
                }else if (listTrayekModel.getTrayek().equals("2A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_2a);
                }else if (listTrayekModel.getTrayek().equals("2B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_2b);
                }else if (listTrayekModel.getTrayek().equals("3A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_3a);
                }else if (listTrayekModel.getTrayek().equals("3B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_3b);
                }else if (listTrayekModel.getTrayek().equals("4A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_4a);
                }else if (listTrayekModel.getTrayek().equals("4B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_4b);
                }else if (listTrayekModel.getTrayek().equals("5A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_5a);
                }else if (listTrayekModel.getTrayek().equals("5B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_5b);
                }else if (listTrayekModel.getTrayek().equals("6A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_6a);
                }else if (listTrayekModel.getTrayek().equals("6B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_6b);
                }else if (listTrayekModel.getTrayek().equals("7")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_7);
                }else if (listTrayekModel.getTrayek().equals("8")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_8);
                }else if (listTrayekModel.getTrayek().equals("9")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_9);
                }else if (listTrayekModel.getTrayek().equals("10")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_10);
                }else if (listTrayekModel.getTrayek().equals("11")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_11);
                }else if (listTrayekModel.getTrayek().equals("Ngemplak (PP)")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_12);
                }else if (listTrayekModel.getTrayek().equals("Ngaglik (PP)")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_13);
                }else {
                    cv_warna.setBackgroundResource(R.drawable.ic_color_14);
                }
            }else if (keterangan.equals("rute terdekat")){

                Integer jarak = Integer.parseInt(listTrayekModel.getDestination().toString());
                if (jarak <= 1000){
                    tv_rute.setText(listTrayekModel.getOrigin() + " - "+ jarak +" meter");
                }else {
                    Integer convert = jarak / 1000;
                    tv_rute.setText(listTrayekModel.getOrigin() + " - "+ convert +" Km");
                }

                if (listTrayekModel.getTrayek().equals("1")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_1a);
                }else if (listTrayekModel.getTrayek().equals("2")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_1b);
                }else if (listTrayekModel.getTrayek().equals("3")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_2a);
                }else if (listTrayekModel.getTrayek().equals("4")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_2b);
                }else if (listTrayekModel.getTrayek().equals("5")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_3a);
                }else if (listTrayekModel.getTrayek().equals("6")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_3b);
                }else if (listTrayekModel.getTrayek().equals("7")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_4a);
                }else if (listTrayekModel.getTrayek().equals("8")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_4b);
                }else if (listTrayekModel.getTrayek().equals("9")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_5a);
                }else if (listTrayekModel.getTrayek().equals("10")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_5b);
                }else if (listTrayekModel.getTrayek().equals("11")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_6a);
                }else if (listTrayekModel.getTrayek().equals("12")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_6b);
                }else if (listTrayekModel.getTrayek().equals("13")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_7);
                }else if (listTrayekModel.getTrayek().equals("14")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_8);
                }else if (listTrayekModel.getTrayek().equals("15")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_9);
                }else if (listTrayekModel.getTrayek().equals("16")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_10);
                }else if (listTrayekModel.getTrayek().equals("17")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_11);
                }else if (listTrayekModel.getTrayek().equals("18")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_12);
                }else if (listTrayekModel.getTrayek().equals("19")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_13);
                }else {
                    cv_warna.setBackgroundResource(R.drawable.ic_color_14);
                }

            }else {

                tv_rute.setText(listTrayekModel.getOrigin() + " - "+ listTrayekModel.getDestination());

                if (listTrayekModel.getTrayek().equals("1A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_1a);
                }else if (tv_trayek.getText().equals("1B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_1b);
                }else if (listTrayekModel.getTrayek().equals("2A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_2a);
                }else if (listTrayekModel.getTrayek().equals("2B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_2b);
                }else if (listTrayekModel.getTrayek().equals("3A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_3a);
                }else if (listTrayekModel.getTrayek().equals("3B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_3b);
                }else if (listTrayekModel.getTrayek().equals("4A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_4a);
                }else if (listTrayekModel.getTrayek().equals("4B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_4b);
                }else if (listTrayekModel.getTrayek().equals("5A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_5a);
                }else if (listTrayekModel.getTrayek().equals("5B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_5b);
                }else if (listTrayekModel.getTrayek().equals("6A")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_6a);
                }else if (listTrayekModel.getTrayek().equals("6B")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_6b);
                }else if (listTrayekModel.getTrayek().equals("7")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_7);
                }else if (listTrayekModel.getTrayek().equals("8")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_8);
                }else if (listTrayekModel.getTrayek().equals("9")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_9);
                }else if (listTrayekModel.getTrayek().equals("10")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_10);
                }else if (listTrayekModel.getTrayek().equals("11")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_11);
                }else if (listTrayekModel.getTrayek().equals("Ngemplak (PP)")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_12);
                }else if (listTrayekModel.getTrayek().equals("Ngaglik (PP)")){
                    cv_warna.setBackgroundResource(R.drawable.ic_color_13);
                }else {
                    cv_warna.setBackgroundResource(R.drawable.ic_color_14);
                }
            }
        }
    }

    public interface ItemAdapterCallback {
    }
}
