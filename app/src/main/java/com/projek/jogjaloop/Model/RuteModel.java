package com.projek.jogjaloop.Model;

public class RuteModel {
    private String nama_rute;
    private double lat;
    private double lon;

    public RuteModel(String nama_rute, double lat, double lon) {
        this.nama_rute = nama_rute;
        this.lat = lat;
        this.lon = lon;
    }

    public String getNama_rute() {
        return nama_rute;
    }

    public void setNama_rute(String nama_rute) {
        this.nama_rute = nama_rute;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
