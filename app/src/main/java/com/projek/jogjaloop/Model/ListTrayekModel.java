package com.projek.jogjaloop.Model;

public class ListTrayekModel {

    private int id;
    private String trayek;
    private String origin;
    private String destination;

    public ListTrayekModel(int id, String trayek, String origin, String destination) {
        this.id = id;
        this.trayek = trayek;
        this.origin = origin;
        this.destination = destination;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTrayek() {
        return trayek;
    }

    public void setTrayek(String trayek) {
        this.trayek = trayek;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
