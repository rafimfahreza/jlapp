package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.TrayekAdapter;
import com.projek.jogjaloop.databinding.ActivityListTrayekBinding;
import com.projek.jogjaloop.databinding.ActivityRuteTerdekatBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class RuteTerdekat extends AppCompatActivity implements TrayekAdapter.ItemAdapterCallback {

    ActivityRuteTerdekatBinding activityRuteTerdekatBinding;

    public static double latitude_pengguna;
    public static double longtitude_pengguna;
    int id_trayek, jarak_lokasi;
    String nama_halte;
    ProgressDialog pd;

    List< HashMap > rute_ = new ArrayList<>();
    List< ListTrayekModel > mapModels = new ArrayList<>();
    List< ListTrayekModel > mapModelsLain = new ArrayList<>();
    TrayekAdapter trayekAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRuteTerdekatBinding = ActivityRuteTerdekatBinding.inflate(getLayoutInflater());
        setContentView(activityRuteTerdekatBinding.getRoot());

        setSupportActionBar(activityRuteTerdekatBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        pd = new ProgressDialog(RuteTerdekat.this);
        pd.setMessage("Menampilkan Trayek...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        TrayekAdapter.keterangan = "rute terdekat";
        loadTerdekat();
    }

    private void loadTerdekat() {
        new JsonTaskListTrayek().execute("http://167.172.143.101/api/GetBaseRoute?nearby=true");
    }

    private class JsonTaskListTrayek extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("allStartingPoint");

                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, Object> detailRute = new HashMap();
                    JSONObject jo = jsonArray.getJSONObject(i);
                    int id = jo.getInt("id");
                    String trayek = jo.getString("trayek");
                    String startPoint = jo.getString("startPoint");
                    double lat = jo.getDouble("lat");
                    double lon = jo.getDouble("lon");
                    String endPoint = jo.getString("endPoint");

                    detailRute.put("id", id);
                    detailRute.put("nama_halte", startPoint);
                    detailRute.put("lat_halte", lat);
                    detailRute.put("lon_halte", lon);
                    detailRute.put("jarak", "0");
                    rute_.add(detailRute);

                    JsonTaskDetail task = new JsonTaskDetail(i);

                    task.execute("https://api.tomtom.com/routing/1/calculateRoute/"+latitude_pengguna+"%2C"+longtitude_pengguna+"%3A"+lat+"%2C"+lon+"/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");

                }
                Log.d("TAG","rute trayek= "+rute_);
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskDetail extends AsyncTask< String, String, String > {

        int i;

        public JsonTaskDetail(int i) {
            this.i = i;
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                JSONObject jo = jsonArray.getJSONObject(0);
                JSONObject summary = jo.getJSONObject("summary");

                Log.d("TAG","Jarak "+summary.getInt("lengthInMeters"));

                rute_.get(this.i).put("jarak", summary.getInt("lengthInMeters"));
                Log.d("TAG","Halaman Rute "+rute_.get(this.i));

                if (mapModels.size() == 0){
                    activityRuteTerdekatBinding.tvTerdekat.setVisibility(View.VISIBLE);
                }else {
                    activityRuteTerdekatBinding.tvTerdekat.setVisibility(View.GONE);
                    activityRuteTerdekatBinding.rvTrayek.setVisibility(View.VISIBLE);
                }

                if (mapModelsLain.size() == 0){
                    activityRuteTerdekatBinding.tvLainnya.setVisibility(View.VISIBLE);
                }else {
                    activityRuteTerdekatBinding.tvLainnya.setVisibility(View.GONE);
                    activityRuteTerdekatBinding.rvLainnya.setVisibility(View.VISIBLE);
                }

                if (Integer.parseInt(rute_.get(i).get("jarak").toString()) <= 1000){
                    mapModels.add(new ListTrayekModel(Integer.parseInt(rute_.get(i).get("id").toString()), rute_.get(i).get("id").toString(), rute_.get(i).get("nama_halte").toString(), rute_.get(i).get("jarak").toString()));
                }else {
                    mapModelsLain.add(new ListTrayekModel(Integer.parseInt(rute_.get(i).get("id").toString()), rute_.get(i).get("id").toString(), rute_.get(i).get("nama_halte").toString(), rute_.get(i).get("jarak").toString()));
                }

                trayekAdapter = new TrayekAdapter(mapModels, RuteTerdekat.this);
                activityRuteTerdekatBinding.rvTrayek.setLayoutManager(new LinearLayoutManager(RuteTerdekat.this));
                activityRuteTerdekatBinding.rvTrayek.setAdapter(trayekAdapter);

                trayekAdapter = new TrayekAdapter(mapModelsLain, RuteTerdekat.this);
                activityRuteTerdekatBinding.rvLainnya.setLayoutManager(new LinearLayoutManager(RuteTerdekat.this));
                activityRuteTerdekatBinding.rvLainnya.setAdapter(trayekAdapter);

                if (mapModels.size() + mapModelsLain.size() > 18){
                    pd.dismiss();
                }

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }

        }
    }
}