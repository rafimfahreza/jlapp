package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.slider.Slider;
import com.projek.jogjaloop.Model.LuncherManager;
import com.projek.jogjaloop.R;

public class SplashScreen extends AppCompatActivity {
    LuncherManager luncherManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getSupportActionBar().hide();
        luncherManager = new LuncherManager(this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(luncherManager.isFirstTime()){
                    luncherManager.setFirstLunch(false);
                    startActivity(new Intent(getApplicationContext(), WelcomePage1.class));
                    finish();
                }else {
                    startActivity(new Intent(getApplicationContext(), BottomNavigationActivity.class));
                    finish();
                }
            }
        }, 3000L); //3000 L = 3 detik
    }
}