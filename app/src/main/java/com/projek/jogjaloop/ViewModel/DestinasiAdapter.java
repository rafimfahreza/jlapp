package com.projek.jogjaloop.ViewModel;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.projek.jogjaloop.Model.DestinasiModel;
import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.Model.RuteModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.View.DetailWisata;
import com.projek.jogjaloop.View.StreetView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DestinasiAdapter extends RecyclerView.Adapter<DestinasiAdapter.ViewHolder> {

    private List< DestinasiModel > rvData;
    private DestinasiAdapter.ItemAdapterCallback itemAdapterCallback;

    public DestinasiAdapter(List< DestinasiModel> destinasiModels, DestinasiAdapter.ItemAdapterCallback mItemAdapterCallback) {
        rvData = destinasiModels;
        itemAdapterCallback = mItemAdapterCallback;
    }

    @NonNull
    @Override
    public DestinasiAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_destinasi, parent, false);
        final DestinasiAdapter.ViewHolder viewHolder = new DestinasiAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DestinasiAdapter.ViewHolder holder, int position) {
        holder.bind(rvData.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailWisata.lat_destinasi = rvData.get(position).getLat();
                DetailWisata.lon_destinasi = rvData.get(position).getLng();
                DetailWisata.nama_lokasi = rvData.get(position).getName();
                DetailWisata.nama_jalan = rvData.get(position).getVicinity();
                Intent intent = new Intent(holder.itemView.getContext(), DetailWisata.class);
                holder.itemView.getContext().startActivity(intent);
                Log.d("TAG", "lat lokasi = "+rvData.get(position).getLat()+"\n Lng lokasi = "+rvData.get(position).getLng());
            }
        });
    }

    @Override
    public int getItemCount() {
        return rvData.size();
    }

    public interface ItemAdapterCallback {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_nama_wisata, tv_lokasi_wisata, tv_rating_wisata;
        ImageView foto_destinasi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_nama_wisata = itemView.findViewById(R.id.tv_nama_wisata);
            tv_lokasi_wisata = itemView.findViewById(R.id.tv_lokasi_wisata);
            tv_rating_wisata = itemView.findViewById(R.id.tv_rating_wisata);
            foto_destinasi = itemView.findViewById(R.id.foto_destinasi);
        }

        void bind(final DestinasiModel destinasiModel){
            tv_nama_wisata.setText(destinasiModel.getName());
            tv_lokasi_wisata.setText(destinasiModel.getVicinity());
            tv_rating_wisata.setText(destinasiModel.getRating());
            Glide.with(itemView).load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+destinasiModel.getPhoto_reference()+"&key=AIzaSyBbm3-vCnZPRS2R_R1VK8U6oAdJOT6b_3I")
            .into(foto_destinasi);
        }
    }
}
