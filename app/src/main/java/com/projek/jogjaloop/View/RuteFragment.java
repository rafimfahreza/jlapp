package com.projek.jogjaloop.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.Model.MapModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.TrayekAdapter;
import com.projek.jogjaloop.databinding.FragmentDashboardBinding;
import com.projek.jogjaloop.databinding.FragmentRuteBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RuteFragment extends Fragment implements TrayekAdapter.ItemAdapterCallback{

    FragmentRuteBinding fragmentRuteBinding;

    private List< MapModel > mapModelList;
    TrayekAdapter trayekAdapter;
    ProgressDialog pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentRuteBinding = FragmentRuteBinding.inflate(inflater, container, false);
        return fragmentRuteBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pd = new ProgressDialog(getContext());
        pd.setMessage("Menampilkan List Trayek...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        loadTrayek();
    }

    private void loadTrayek() {
        new JsonTaskListTrayek().execute("http://167.172.143.101/api/GetBaseRoute");

        TrayekAdapter.keterangan = "rute_fragment";
    }

    private class JsonTaskListTrayek extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            List< ListTrayekModel > mapModels = new ArrayList<>();
            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("trayek");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    int id = jo.getInt("id");
                    String trayek = jo.getString("trayek");
                    String origin = jo.getString("origin");
                    String destination = jo.getString("destination");

                    Log.d("TAG","trayek = "+jo.getString("trayek"));
                    mapModels.add(new ListTrayekModel(id, trayek, origin, destination));
                }
                trayekAdapter = new TrayekAdapter(mapModels, RuteFragment.this);
                fragmentRuteBinding.rvTrayek.setLayoutManager(new LinearLayoutManager(getContext()));
                fragmentRuteBinding.rvTrayek.setAdapter(trayekAdapter);
                pd.dismiss();
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }
}