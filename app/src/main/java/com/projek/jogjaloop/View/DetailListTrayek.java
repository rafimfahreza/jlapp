package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.Model.MapModel;
import com.projek.jogjaloop.Model.RuteModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.RuteAdapter;
import com.projek.jogjaloop.ViewModel.TrayekAdapter;
import com.projek.jogjaloop.databinding.ActivityDetailListTrayekBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DetailListTrayek extends AppCompatActivity implements OnMapReadyCallback, RuteAdapter.ItemAdapterCallback {


    List< RuteModel > ruteModelList = new ArrayList<>();
    RuteAdapter ruteAdapter;
    ProgressDialog pd;

    private ActivityDetailListTrayekBinding activityDetailListTrayekBinding;

    public static String keterangan;
    public static int id_trayek;
    public static String nama_trayek;
    public static String origin;
    public static String destination;

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;
    private View mapView;
    Polyline polyline;
    private final float DEFAULT_ZOOM = 14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityDetailListTrayekBinding = ActivityDetailListTrayekBinding.inflate(getLayoutInflater());
        setContentView(activityDetailListTrayekBinding.getRoot());

        setSupportActionBar(activityDetailListTrayekBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        pd = new ProgressDialog(DetailListTrayek.this);
        pd.setMessage("Menampilkan arah...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        loadDetail();
        getMaps();
        loadPolyline();
    }

    private void loadDetail() {

        if (keterangan.equals("rute terdekat")){
            TextView cv_warna = activityDetailListTrayekBinding.cvWarna;
            if (nama_trayek.equals("1")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_1a);
            } else if (nama_trayek.equals("2")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_1b);
            } else if (nama_trayek.equals("3")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_2a);
            } else if (nama_trayek.equals("4")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_2b);
            } else if (nama_trayek.equals("5")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_3a);
            } else if (nama_trayek.equals("6")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_3b);
            } else if (nama_trayek.equals("7")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_4a);
            } else if (nama_trayek.equals("8")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_4b);
            } else if (nama_trayek.equals("9")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_5a);
            } else if (nama_trayek.equals("10")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_5b);
            } else if (nama_trayek.equals("11")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_6a);
            } else if (nama_trayek.equals("12")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_6b);
            } else if (nama_trayek.equals("13")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_7);
            } else if (nama_trayek.equals("14")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_8);
            } else if (nama_trayek.equals("15")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_9);
            } else if (nama_trayek.equals("16")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_10);
            } else if (nama_trayek.equals("17")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_11);
            } else if (nama_trayek.equals("18")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_12);
            } else if (nama_trayek.equals("19")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_13);
            } else {
                cv_warna.setBackgroundResource(R.drawable.ic_color_14);
            }

            activityDetailListTrayekBinding.tvTrayek.setText("Trayek " + nama_trayek);
            activityDetailListTrayekBinding.tvRute.setText(origin);
        }else {
            TextView cv_warna = activityDetailListTrayekBinding.cvWarna;
            if (nama_trayek.equals("1A")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_1a);
            } else if (nama_trayek.equals("1B")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_1b);
            } else if (nama_trayek.equals("2A")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_2a);
            } else if (nama_trayek.equals("2B")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_2b);
            } else if (nama_trayek.equals("3A")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_3a);
            } else if (nama_trayek.equals("3B")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_3b);
            } else if (nama_trayek.equals("4A")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_4a);
            } else if (nama_trayek.equals("4B")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_4b);
            } else if (nama_trayek.equals("5A")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_5a);
            } else if (nama_trayek.equals("5B")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_5b);
            } else if (nama_trayek.equals("6A")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_6a);
            } else if (nama_trayek.equals("6B")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_6b);
            } else if (nama_trayek.equals("7")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_7);
            } else if (nama_trayek.equals("8")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_8);
            } else if (nama_trayek.equals("9")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_9);
            } else if (nama_trayek.equals("10")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_10);
            } else if (nama_trayek.equals("11")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_11);
            } else if (nama_trayek.equals("Ngemplak (PP)")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_12);
            } else if (nama_trayek.equals("Ngaglik (PP)")) {
                cv_warna.setBackgroundResource(R.drawable.ic_color_13);
            } else {
                cv_warna.setBackgroundResource(R.drawable.ic_color_14);
            }

            activityDetailListTrayekBinding.tvTrayek.setText("Trayek " + nama_trayek);
            activityDetailListTrayekBinding.tvRute.setText(origin + " - " + destination);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    private void getMaps() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(DetailListTrayek.this);
        Places.initialize(DetailListTrayek.this, getString(R.string.api_key));
        placesClient = Places.createClient(this);
    }

    private void loadPolyline() {
        new JsonTaskRoutes().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek);
    }

    private class JsonTaskRoutes extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
//                new JsonTaskRoutes().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek);
                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();
                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    ruteModelList.add(new RuteModel(users.getString("name"),rutes,rutess));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesHalte().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

                ruteAdapter = new RuteAdapter(ruteModelList, DetailListTrayek.this);
                activityDetailListTrayekBinding.rvRute.setLayoutManager(new LinearLayoutManager(DetailListTrayek.this));
                activityDetailListTrayekBinding.rvRute.setAdapter(ruteAdapter);

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesHalte extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#FFA866"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}