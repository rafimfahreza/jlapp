package com.projek.jogjaloop.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.projek.jogjaloop.Model.RuteModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.RuteAdapter;
import com.projek.jogjaloop.databinding.ActivitySearchDanBisKlikBinding;
import com.projek.jogjaloop.databinding.EmptyRuteBinding;
import com.projek.jogjaloop.databinding.EstimasiWaktuBinding;
import com.projek.jogjaloop.databinding.RuteTrayekBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class SearchDanBisKlik extends AppCompatActivity implements OnMapReadyCallback, RuteAdapter.ItemAdapterCallback {

    private CheckBox cb_1a, cb_1b, cb_2a, cb_2b, cb_3a, cb_3b, cb_4a, cb_4b, cb_5a, cb_5b, cb_6a, cb_6b, cb_7, cb_8, cb_9, cb_10, cb_11, cb_12, cb_13, cb_14;

    Integer jumlah_pilih = 0;

    private GoogleMap mMap, maps, map_line;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;
    private View mapView;
    private Polyline polyline;
    private Polyline polyline_bis;
    Integer jumlah_cari = 0;
    Integer jumlah_klik = 0;
    Marker markerOptions;
    Marker markerOptions_bis;

    public static String keterangan, nama_lokasi, nama_trayek_bus;
    String keterangan_1 = "";
    String keterangan_lokasi = "lokasi_unklik";
    double latitude_bis, longtitude_bis;
    public static double lat_pengguna, long_pengguna, lat_search, long_search, lat_bis_titik, long_bis_titik;
    public static int id_bis;

    String departureTime;
    String arrivalTime;
    Integer travelTimeInSeconds;

    private ActivitySearchDanBisKlikBinding activitySearchDanBisKlikBinding;
    private EmptyRuteBinding emptyRuteBinding;
    private RuteTrayekBinding ruteTrayekBinding;
    private EstimasiWaktuBinding estimasiWaktuBinding;

    ProgressDialog pd;

    List< RuteModel > ruteModelList = new ArrayList<>();
    RuteAdapter ruteAdapter;

    List <Polyline> plArray = new ArrayList<Polyline>();
    List <Marker> plArray_marker = new ArrayList<Marker>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activitySearchDanBisKlikBinding = ActivitySearchDanBisKlikBinding.inflate(getLayoutInflater());
        setContentView(activitySearchDanBisKlikBinding.getRoot());

        emptyRuteBinding = activitySearchDanBisKlikBinding.emptyRute;
        ruteTrayekBinding = activitySearchDanBisKlikBinding.ruteTrayek;
        estimasiWaktuBinding = activitySearchDanBisKlikBinding.estimasiPerjalanan;

        setSupportActionBar(activitySearchDanBisKlikBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Log.d("TAG","Keterangan = "+keterangan);

        pd = new ProgressDialog(SearchDanBisKlik.this);
        pd.setMessage("Menampilkan arah...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        getMaps();
        getCheckBoxTrayek();
    }

    private void getCheckBoxTrayek() {
        cb_1a = activitySearchDanBisKlikBinding.cb1a;
        cb_1b = activitySearchDanBisKlikBinding.cb1b;
        cb_2a = activitySearchDanBisKlikBinding.cb2a;
        cb_2b = activitySearchDanBisKlikBinding.cb2b;
        cb_3a = activitySearchDanBisKlikBinding.cb3a;
        cb_3b = activitySearchDanBisKlikBinding.cb3b;
        cb_4a = activitySearchDanBisKlikBinding.cb4a;
        cb_4b = activitySearchDanBisKlikBinding.cb4b;
        cb_5a = activitySearchDanBisKlikBinding.cb5a;
        cb_5b = activitySearchDanBisKlikBinding.cb5b;
        cb_6a = activitySearchDanBisKlikBinding.cb6a;
        cb_6b = activitySearchDanBisKlikBinding.cb6b;
        cb_7 = activitySearchDanBisKlikBinding.cb7;
        cb_8 = activitySearchDanBisKlikBinding.cb8;
        cb_9 = activitySearchDanBisKlikBinding.cb9;
        cb_10 = activitySearchDanBisKlikBinding.cb10;
        cb_11 = activitySearchDanBisKlikBinding.cb11;
        cb_12 = activitySearchDanBisKlikBinding.cb12;
        cb_13 = activitySearchDanBisKlikBinding.cb13;
        cb_14 = activitySearchDanBisKlikBinding.cb14;

        activitySearchDanBisKlikBinding.btnPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cb_1a.isChecked() && !cb_1b.isChecked() && !cb_2a.isChecked() && !cb_2b.isChecked() && !cb_3a.isChecked()
                        && !cb_3b.isChecked() && !cb_4a.isChecked() && !cb_4b.isChecked() && !cb_5a.isChecked() && !cb_5b.isChecked()
                        && !cb_6a.isChecked() && !cb_6b.isChecked() && !cb_7.isChecked() && !cb_8.isChecked() && !cb_9.isChecked()
                        && !cb_10.isChecked() && !cb_11.isChecked() && !cb_12.isChecked() && !cb_13.isChecked() && !cb_14.isChecked())
                {
                    Snackbar snackbar = Snackbar.make(activitySearchDanBisKlikBinding.layout, "Belum ada trayek yg dipilih.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else {

                    if (cb_1a.isChecked()){
                        FilterPolylineDanEstimasi.trayek_1a = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_1a = 1;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 1;
                        FilterPolylineDanEstimasi.drawable_trayek_1a = R.drawable.ic_color_1a;

                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_1b.isChecked()){
                        FilterPolylineDanEstimasi.trayek_1b = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_1b = 2;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 2;
                        FilterPolylineDanEstimasi.drawable_trayek_1b = R.drawable.ic_color_1b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_2a.isChecked()){
                        FilterPolylineDanEstimasi.trayek_2a = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_2a = 3;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 3;
                        FilterPolylineDanEstimasi.drawable_trayek_2a = R.drawable.ic_color_2a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_2b.isChecked()){
                        FilterPolylineDanEstimasi.trayek_2b = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_2b = 4;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 4;
                        FilterPolylineDanEstimasi.drawable_trayek_2b = R.drawable.ic_color_2b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_3a.isChecked()){
                        FilterPolylineDanEstimasi.trayek_3a = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_3a = 5;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 5;
                        FilterPolylineDanEstimasi.drawable_trayek_3a = R.drawable.ic_color_3a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_3b.isChecked()){
                        FilterPolylineDanEstimasi.trayek_3b = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_3b = 6;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 6;
                        FilterPolylineDanEstimasi.drawable_trayek_3b = R.drawable.ic_color_3b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_4a.isChecked()){
                        FilterPolylineDanEstimasi.trayek_4a = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_4a = 7;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 7;
                        FilterPolylineDanEstimasi.drawable_trayek_4a = R.drawable.ic_color_4a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_4b.isChecked()){
                        FilterPolylineDanEstimasi.trayek_4b = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_4b = 8;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 8;
                        FilterPolylineDanEstimasi.drawable_trayek_4b = R.drawable.ic_color_4b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_5a.isChecked()){
                        FilterPolylineDanEstimasi.trayek_5a = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_5a = 9;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 9;
                        FilterPolylineDanEstimasi.drawable_trayek_5a = R.drawable.ic_color_5b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_5b.isChecked()){
                        FilterPolylineDanEstimasi.trayek_5b = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_5b = 10;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 10;
                        FilterPolylineDanEstimasi.drawable_trayek_5b = R.drawable.ic_color_5b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_6a.isChecked()){
                        FilterPolylineDanEstimasi.trayek_6a = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_6a = 11;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 11;
                        FilterPolyline.drawable_trayek_6a = R.drawable.ic_color_6a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_6b.isChecked()){
                        FilterPolylineDanEstimasi.trayek_6b = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_6b = 12;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 12;
                        FilterPolylineDanEstimasi.drawable_trayek_6b = R.drawable.ic_color_6b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_7.isChecked()){
                        FilterPolylineDanEstimasi.trayek_7 = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_7 = 13;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 13;
                        FilterPolylineDanEstimasi.drawable_trayek_7 = R.drawable.ic_color_7;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_8.isChecked()){
                        FilterPolylineDanEstimasi.trayek_8 = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_8 = 14;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 14;
                        FilterPolylineDanEstimasi.drawable_trayek_8 = R.drawable.ic_color_8;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_9.isChecked()){
                        FilterPolylineDanEstimasi.trayek_9 = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_9 = 15;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 15;
                        FilterPolylineDanEstimasi.drawable_trayek_9 = R.drawable.ic_color_9;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_10.isChecked()){
                        FilterPolylineDanEstimasi.trayek_10 = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_10 = 16;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 16;
                        FilterPolylineDanEstimasi.drawable_trayek_10 = R.drawable.ic_color_10;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_11.isChecked()){
                        FilterPolylineDanEstimasi.trayek_11 = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_11 = 17;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 17;
                        FilterPolylineDanEstimasi.drawable_trayek_11 = R.drawable.ic_color_11;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_12.isChecked()){
                        FilterPolylineDanEstimasi.trayek_12 = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_12 = 18;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 18;
                        FilterPolylineDanEstimasi.drawable_trayek_12 = R.drawable.ic_color_12;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_13.isChecked()){
                        FilterPolylineDanEstimasi.trayek_13 = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_13 = 19;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 19;
                        FilterPolylineDanEstimasi.drawable_trayek_13 = R.drawable.ic_color_13;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_14.isChecked()){
                        FilterPolylineDanEstimasi.trayek_14 = "pilih";
                        FilterPolylineDanEstimasi.id_trayek_14 = 20;
                        FilterPolylineDanEstimasi.id_trayek_pilih_polyline = 20;
                        FilterPolylineDanEstimasi.drawable_trayek_14 = R.drawable.ic_color_14;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    FilterPolylineDanEstimasi.nama_lokasi = nama_lokasi;
                    FilterPolylineDanEstimasi.lat_search = lat_search;
                    FilterPolylineDanEstimasi.long_search = long_search;
                    FilterPolylineDanEstimasi.jumlah_pilih = jumlah_pilih;
                    startActivity(new Intent(getApplicationContext(), FilterPolylineDanEstimasi.class));
                    finish();
                }
            }
        });
    }

    private void onLokasi(String ket) {

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.api_key), Locale.US);
        }

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));
        autocompleteFragment.setHint("Cari Lokasi...");

        if (ket.equals("lokasi")){
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(@NonNull Place place) {
                    activitySearchDanBisKlikBinding.layoutEstimasi.setVisibility(View.VISIBLE);
                    if (jumlah_cari > 0){
                        polyline.remove();
                        markerOptions.remove();
                    }
                    lat_search = place.getLatLng().latitude;
                    long_search = place.getLatLng().longitude;
                    nama_lokasi = place.getName();
                    new JsonTaskDetailEstimasi().execute("https://api.tomtom.com/routing/1/calculateRoute/"+lat_pengguna+"%2C"+long_pengguna+"%3A"+lat_search+"%2C"+long_search+"/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                    new JsonTaskRoutesSearch().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_pengguna + "%2C" + long_pengguna + "%3A" + lat_search + "%2C" + long_search + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat_search, long_search), 12));
                    LatLng m1 = new LatLng(lat_search, long_search);
                    markerOptions  = mMap.addMarker(new MarkerOptions()
                            .position(m1)
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.pin_dituju))
                            .title(place.getName()));
                    activitySearchDanBisKlikBinding.estimasiPerjalanan.tvLokasiTujuan.setText(nama_lokasi);
                    Log.i("TAG", "Place: " + place.getName() + ", " + place.getId()+"lat "+lat_search +"Lng "+long_search);

                    activitySearchDanBisKlikBinding.layoutEmpytState.setVisibility(View.GONE);
                    activitySearchDanBisKlikBinding.layoutRuteTrayek.setVisibility(View.GONE);

                    onLokasi("lokasi");
                }

                @Override
                public void onError(@NonNull Status status) {
                    Log.i("TAG", "An error occurred: " + status);
                }
            });
        }else {
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(@NonNull Place place) {
                    activitySearchDanBisKlikBinding.layoutEstimasi.setVisibility(View.VISIBLE);

                    activitySearchDanBisKlikBinding.estimasiPerjalanan.tvLokasiTujuan.setText(place.getName());

                    if (jumlah_cari > 0){
                        polyline.remove();
                        markerOptions.remove();
                    }
                    LatLng m1 = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                    markerOptions = mMap.addMarker(new MarkerOptions()
                            .position(m1)
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.pin_dituju))
                            .title(place.getName()));
                    new JsonTaskDetailEstimasi().execute("https://api.tomtom.com/routing/1/calculateRoute/"+lat_pengguna+"%2C"+long_pengguna+"%3A"+place.getLatLng().latitude+"%2C"+place.getLatLng().longitude+"/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(place.getLatLng().latitude, place.getLatLng().longitude), 12));

                    new JsonTaskRoutesSearch().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_pengguna + "%2C" + long_pengguna + "%3A" + place.getLatLng().latitude + "%2C" + place.getLatLng().longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");

                    onLokasi("bis");
                }

                @Override
                public void onError(@NonNull Status status) {
                    Log.i("TAG", "An error occurred: " + status);
                }
            });
        }

        getLocationBus();
    }

    private void getMaps() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(SearchDanBisKlik.this);
        Places.initialize(SearchDanBisKlik.this, getString(R.string.api_key));
        placesClient = Places.createClient(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        if (keterangan.equals("lokasi")){
            activitySearchDanBisKlikBinding.layoutEmpytState.setVisibility(View.GONE);
            activitySearchDanBisKlikBinding.layoutEstimasi.setVisibility(View.VISIBLE);
//            activitySearchDanBisKlikBinding.layoutRuteTrayek.setVisibility(View.GONE);
            activitySearchDanBisKlikBinding.estimasiPerjalanan.tvLokasiTujuan.setText(nama_lokasi);

            new JsonTaskDetailEstimasi().execute("https://api.tomtom.com/routing/1/calculateRoute/"+lat_pengguna+"%2C"+long_pengguna+"%3A"+lat_search+"%2C"+long_search+"/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");

            new JsonTaskRoutesSearch().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_pengguna + "%2C" + long_pengguna + "%3A" + lat_search + "%2C" + long_search + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat_search, long_search), 12));
            LatLng m1 = new LatLng(lat_search, long_search);
            markerOptions = mMap.addMarker(new MarkerOptions()
                    .position(m1)
                    .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.pin_dituju))
                    .title(nama_lokasi));

            onLokasi("lokasi");

        }else if (keterangan.equals("bis")){
            activitySearchDanBisKlikBinding.layoutRuteTrayek.setVisibility(View.VISIBLE);
            activitySearchDanBisKlikBinding.ruteTrayek.tvNamaTrayek.setText(nama_trayek_bus);
            new JsonTaskRoutesPerjalanan().execute("http://167.172.143.101/api/GetAllBus?id=" + id_bis);
            new JsonTaskRoutes_bis().execute("http://167.172.143.101/api/GetAllBus?id="+id_bis);
            onLokasi("bis");
        }

        getLocationBus();
    }

    private void getLocationBus() {
        new JsonTaskLocationBus().execute("http://167.172.143.101/api/GetAllBus");

//        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//            @Override
//            public void onInfoWindowClick(Marker marker) {
//                String substring = marker.getTitle().substring(0, 3);
//                String id_bus_pilih = marker.getTitle().substring(4, 5);
//                if (substring.equals("Bus")){
//                    activitySearchDanBisKlikBinding.layoutRuteTrayek.setVisibility(View.VISIBLE);
//                    activitySearchDanBisKlikBinding.layoutEmpytState.setVisibility(View.GONE);
//                    if (polyline_bis != null){
//                        for (int i=0 ; i< plArray.size(); i++){
//                            plArray.get(i).remove();
//                        }
//
//                        for (int j=0 ; j< plArray_marker.size(); j++){
//                            plArray_marker.get(j).remove();
//                        }
//                        markerOptions_bis.remove();
//                    }else {
//                        Log.d("TAG","tidak diklik");
//                    }
//
//                    keterangan_1 = "sudah";
//                    if (keterangan.equals("lokasi")){
//                        if (ruteModelList.size() <= 0){
//
//                        }else {
//                            ruteModelList.clear();
//                            ruteAdapter.notifyDataSetChanged();
//                        }
//                    }else {
//                        ruteModelList.clear();
//                        ruteAdapter.notifyDataSetChanged();
//                    }
//                    new JsonTaskRoutes_bis().execute("http://167.172.143.101/api/GetAllBus?id="+Integer.valueOf(id_bus_pilih));
//                    new JsonTaskRoutesPerjalanan().execute("http://167.172.143.101/api/GetAllBus?id=" + id_bus_pilih);
//                    activitySearchDanBisKlikBinding.ruteTrayek.tvNamaTrayek.setText(marker.getSnippet());
//                    getLocationBus();
//                    pd.show();
//                    pd.setMessage("Memasukkan rute");
//                    onLokasi("bis");
//                }else {
//                    Log.d("TAG", "anda tidak memilih icon bus");
//                }
//                return;
//            }
//        });
    }

    private class JsonTaskLocationBus extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("buses");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    double lat = jo.getDouble("lat");
                    double lon = jo.getDouble("lon");
                    int id = jo.getInt("id");
                    String noBus = jo.getString("noBus");
                    String driver = jo.getString("driver");
                    String trayek = jo.getString("trayek");

                    if (trayek.equals("1A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_1a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("1B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_1b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("2A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_2a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("2B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_2b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("3A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_3a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("3B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_3b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("4A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_4a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("4B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_4b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("5A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_5a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("5B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_5b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("6A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_6a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("6B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_6b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("7")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_7))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("8")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_8))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("9")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_9))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("10")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_10))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("11")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_11))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("12")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_12))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("13")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_13))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else {
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_color_14))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));
                pd.dismiss();

            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private class JsonTaskRoutes_bis extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("detail");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    JSONArray route = users.getJSONArray("route");

                    for (int j=0; j<route.length(); j++){
                        JSONObject summary = route.getJSONObject(j);
                        double rutes = summary.getDouble("lat");
                        double rutess = summary.getDouble("lon");

                        LatLng mw = new LatLng(rutes, rutess);
                        lat_lon.add(mw);

                        markerOptions_bis = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(rutes, rutess))
                                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                                .title(summary.getString("name")));

                        plArray_marker.add(markerOptions_bis);

                        Log.d("TAG", "Hasil latitude = " + rutes);
                        Log.d("TAG", "Hasil longtitude = " + rutess);
                    }

                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));
                for (int m=0; m<lat_lon.size() - 1; m++){
                    new JsonTaskRoutesbisPolyline().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(m).latitude + "%2C" + lat_lon.get(m).longitude + "%3A" + lat_lon.get(m + 1).latitude + "%2C" + lat_lon.get(m + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }
                jumlah_klik = jumlah_klik + 1;
                pd.dismiss();
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }

        }
    }

    private class JsonTaskRoutesbisPolyline extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions options = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            options.color(Color.parseColor("#FFA866"));
                            options.width(10);
                            options.geodesic(true);
                            options.add(new LatLng(rutes, rutess));

                            Log.d("TAG", "Hasil latitude = " + rutes);
                            Log.d("TAG", "Hasil longtitude = " + rutess);
                            jumlah_klik = jumlah_klik + 1;
                        }
                        polyline_bis = mMap.addPolyline(options);
                        plArray.add(polyline_bis);
                    }
                }
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
            pd.dismiss();
        }
    }

    private class JsonTaskRoutesSearch extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptions = new PolylineOptions();
                        double rutes = 0.0;
                        double rutess = 0.0;
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            rutes = users.getDouble("latitude");
                            rutess = users.getDouble("longitude");
                            polylineOptions.color(Color.parseColor("#4bc5d2"));
                            polylineOptions.width(10);
                            polylineOptions.add(new LatLng(rutes, rutess));

                            Log.d("TAG", "Hasil latitude = " + rutes);
                            Log.d("TAG", "Hasil longtitude = " + rutess);
                        }
                        polyline = mMap.addPolyline(polylineOptions);
                        jumlah_cari = jumlah_cari + 1;
                    }
                }

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
            pd.dismiss();
        }
    }

    private class JsonTaskRoutesPerjalanan extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("detail");

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    JSONArray route = users.getJSONArray("route");
                    for (int j=0; j<route.length(); j++){
                        JSONObject summary = route.getJSONObject(j);
                        double rutes = summary.getDouble("lat");
                        double rutess = summary.getDouble("lon");
                        String name = summary.getString("name");

                        Log.d("TAG", "Hasil latitude rute = " + rutes);
                        Log.d("TAG", "Hasil longtitude rute = " + rutess);

                        ruteModelList.add(new RuteModel(name,rutes,rutess));

                        if (j == 0){
                            activitySearchDanBisKlikBinding.ruteTrayek.tvRuteTrayek.setText(name+" - ");
                        }

                        if (j == route.length() - 3){
                            activitySearchDanBisKlikBinding.ruteTrayek.tvArrival.setText(name);
                        }
                    }

                }

                ruteAdapter = new RuteAdapter(ruteModelList, SearchDanBisKlik.this);
                activitySearchDanBisKlikBinding.ruteTrayek.rvRute.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                activitySearchDanBisKlikBinding.ruteTrayek.rvRute.setAdapter(ruteAdapter);
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskDetailEstimasi extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONObject summary = jo.getJSONObject("summary");
                    departureTime = summary.getString("departureTime").substring(11, 16);
                    arrivalTime = summary.getString("arrivalTime").substring(11, 16);
                    travelTimeInSeconds = summary.getInt("travelTimeInSeconds");
                }

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
            Integer waktu = travelTimeInSeconds / 60;
            activitySearchDanBisKlikBinding.estimasiPerjalanan.tvEstimasiWaktu.setText(""+waktu);
            activitySearchDanBisKlikBinding.estimasiPerjalanan.tvJamDatang.setText(departureTime);
            activitySearchDanBisKlikBinding.estimasiPerjalanan.tvSampai.setText(arrivalTime);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(getApplicationContext(), BottomNavigationActivity.class));
        finish();
    }
}