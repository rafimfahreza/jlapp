package com.projek.jogjaloop.View;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.support.v4.media.MediaBrowserCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.databinding.FragmentDashboardBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class  DashboardFragment extends Fragment implements OnMapReadyCallback{

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;
    private View mapView;
    private final float DEFAULT_ZOOM = 15;

    //Floating Action Button
    FloatingActionButton mAddAlarmFab, mAddPersonFab, mRuteTerdekat, fb_destinasi;
    ExtendedFloatingActionButton mAddFab;
    TextView addAlarmActionText, addPersonActionText, tv_rute_terdekat, tv_destinasi;
    Boolean isAllFabsVisible;

    ProgressDialog pd;
    private FragmentDashboardBinding fragmentDashboardBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        fragmentDashboardBinding = FragmentDashboardBinding.inflate(inflater, container, false);
        return fragmentDashboardBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pd = new ProgressDialog(getContext());
        pd.setMessage("Menampilkan Maps...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        checkPermission();
        ActionButton();
        onLokasi();
        circleBound();
    }

    private void onLokasi() {
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), getString(R.string.api_key), Locale.US);
        }

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) this.getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));
        autocompleteFragment.setHint("Cari Lokasi...");
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {

                SearchDanBisKlik.keterangan = "lokasi";
                SearchDanBisKlik.nama_lokasi = place.getName();
                SearchDanBisKlik.lat_search = place.getLatLng().latitude;
                SearchDanBisKlik.long_search = place.getLatLng().longitude;

                startActivity(new Intent(getContext(), SearchDanBisKlik.class));
//                Toast.makeText(getContext(), ""+place.getName(), Toast.LENGTH_SHORT).show();
                Log.i("TAG", "Place: " + place.getName() + ", " + place.getId()+"lat "+place.getLatLng().latitude +"Lng "+place.getLatLng().longitude);
            }

            @Override
            public void onError(@NonNull Status status) {
                Log.i("TAG", "An error occurred: " + status);
            }
        });
    }

    private void ActionButton() {
        mAddFab = fragmentDashboardBinding.addFab;
        mAddAlarmFab = fragmentDashboardBinding.addAlarmFab;
        mAddPersonFab = fragmentDashboardBinding.addPersonFab;
        fb_destinasi = fragmentDashboardBinding.fbDestinasi;
        addAlarmActionText = fragmentDashboardBinding.addAlarmActionText;
        addPersonActionText = fragmentDashboardBinding.addPersonActionText;
        tv_rute_terdekat = fragmentDashboardBinding.tvRuteTerdekat;
        tv_destinasi = fragmentDashboardBinding.tvDestinasi;
        mRuteTerdekat = fragmentDashboardBinding.ruteTerdekat;

        mAddAlarmFab.setVisibility(View.GONE);
        mAddPersonFab.setVisibility(View.GONE);
        mRuteTerdekat.setVisibility(View.GONE);
        fb_destinasi.setVisibility(View.GONE);
        addAlarmActionText.setVisibility(View.GONE);
        addPersonActionText.setVisibility(View.GONE);
        tv_rute_terdekat.setVisibility(View.GONE);
        tv_destinasi.setVisibility(View.GONE);
        isAllFabsVisible = false;
        mAddFab.shrink();

        mAddFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isAllFabsVisible) {
                            mAddAlarmFab.show();
                            mAddPersonFab.show();
                            mRuteTerdekat.show();
                            fb_destinasi.show();
                            addAlarmActionText.setVisibility(View.VISIBLE);
                            addPersonActionText.setVisibility(View.VISIBLE);
                            tv_rute_terdekat.setVisibility(View.VISIBLE);
                            tv_destinasi.setVisibility(View.VISIBLE);

                            mAddFab.extend();

                            isAllFabsVisible = true;
                        } else {
                            mAddAlarmFab.hide();
                            mAddPersonFab.hide();
                            mRuteTerdekat.hide();
                            fb_destinasi.hide();
                            addAlarmActionText.setVisibility(View.GONE);
                            addPersonActionText.setVisibility(View.GONE);
                            tv_rute_terdekat.setVisibility(View.GONE);
                            tv_destinasi.setVisibility(View.GONE);

                            mAddFab.shrink();

                            isAllFabsVisible = false;
                        }
                    }
                });
        mAddPersonFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), FilterTrayek.class));
                    }
                });
        mAddAlarmFab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), ListTrayek.class));
                    }
                });
        mRuteTerdekat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), RuteTerdekat.class));
            }
        });

        fb_destinasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), DestinasiWisata.class));
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 180);
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        Task< LocationSettingsResponse > task = settingsClient.checkLocationSettings(builder.build());

        task.addOnSuccessListener(getActivity(), new OnSuccessListener< LocationSettingsResponse >() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getDeviceLocation();
            }
        });

        task.addOnFailureListener(getActivity(), new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        resolvable.startResolutionForResult(getActivity(), 51);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        getLocationBus();
        //-7.808739308888561, 110.37750359785072
        //-7.808569239274224, 110.3775250555206
    }

    private void circleBound(){
        boolean isInside = Math.pow(-7.808569239274224  - -7.808739308888561, 2) + Math.pow(110.3775250555206 - 110.37750359785072, 2) >= 0.001*0.001;
        Log.d("TAG","isInside= "+isInside);
    }

    private void getLocationBus() {
        new JsonTaskLocationBus().execute("http://167.172.143.101/api/GetAllBus");
    }

    private class JsonTaskLocationBus extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("buses");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    double lat = jo.getDouble("lat");
                    double lon = jo.getDouble("lon");
                    int id = jo.getInt("id");
                    String noBus = jo.getString("noBus");
                    String driver = jo.getString("driver");
                    String trayek = jo.getString("trayek");

                    if (trayek.equals("1A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_1a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("1B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_1b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("2A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_2a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("2B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_2b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("3A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_3a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("3B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_3b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("4A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_4a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("4B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_4b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("5A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_5a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("5B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_5b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("6A")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_6a))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("6B")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_6b))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("7")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_7))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("8")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_8))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("9")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_9))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("10")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_10))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("11")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_11))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("12")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_12))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else if (trayek.equals("13")){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_13))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }else {
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lon))
                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_color_14))
                                .title("Bus "+id+", "+noBus)
                                .snippet("Trayek "+trayek));
                    }

                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));
                pd.dismiss();

//                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//                    @Override
//                    public void onInfoWindowClick(Marker marker) {
//                        SearchDanBisKlik.keterangan = "bis";
//                        String substring = marker.getTitle().substring(4, 5);
//                        SearchDanBisKlik.lat_bis_titik = marker.getPosition().latitude;
//                        SearchDanBisKlik.long_bis_titik = marker.getPosition().latitude;
//                        SearchDanBisKlik.id_bis = Integer.parseInt(substring);
//                        SearchDanBisKlik.nama_trayek_bus = marker.getSnippet();
//                        startActivity(new Intent(getContext(), SearchDanBisKlik.class));
//                    }
//                });

            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private void checkPermission() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        getMaps();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Permission Denied")
                                    .setMessage("Permission to access device location is permanently denied. you need to go to setting to allow the permission.")
                                    .setNegativeButton("Cancel", null)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent();
                                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            intent.setData(Uri.fromParts("package", getActivity().getPackageName(), null));
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    private void getMaps() {
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        Places.initialize(getContext(), getString(R.string.api_key));
        placesClient = Places.createClient(getContext());
    }

    private void getDeviceLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener< Location >() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onComplete(@NonNull Task< Location > task) {
                        if (task.isSuccessful()) {

                            Location location = task.getResult();
                            if (location != null){
                                try {
                                    Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                                    List< Address > addresses = geocoder.getFromLocation(
                                            location.getLatitude(), location.getLongitude(), 1
                                    );
                                    SearchDanBisKlik.lat_pengguna = addresses.get(0).getLatitude();
                                    SearchDanBisKlik.long_pengguna = addresses.get(0).getLongitude();
                                    FilterPolylineDanEstimasi.lat_pengguna = addresses.get(0).getLatitude();
                                    FilterPolylineDanEstimasi.long_pengguna = addresses.get(0).getLongitude();
                                    RuteTerdekat.latitude_pengguna = addresses.get(0).getLatitude();
                                    RuteTerdekat.longtitude_pengguna = addresses.get(0).getLongitude();
                                    JadwalFragment.lat_pengguna = addresses.get(0).getLatitude();
                                    JadwalFragment.lon_pengguna = addresses.get(0).getLongitude();
                                    DetailWisata.lat_pengguna = addresses.get(0).getLatitude();
                                    DetailWisata.long_pengguna = addresses.get(0).getLongitude();
                                    Log.d("TAG","Lokasi saat ini: "+addresses.get(0).getAddressLine(0));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            mLastKnownLocation = task.getResult();
                            if (mLastKnownLocation != null) {
//                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            } else {
                                final LocationRequest locationRequest = LocationRequest.create();
                                locationRequest.setInterval(10000);
                                locationRequest.setFastestInterval(5000);
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                locationCallback = new LocationCallback() {
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if (locationResult == null) {
                                            return;
                                        }
                                        mLastKnownLocation = locationResult.getLastLocation();
//                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
                                    }
                                };
                                mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        } else {
                            Toast.makeText(getContext(), "unable to get last location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}