package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.Model.MapModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.TrayekAdapter;
import com.projek.jogjaloop.databinding.ActivityListTrayekBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class ListTrayek extends AppCompatActivity implements TrayekAdapter.ItemAdapterCallback {

    private ActivityListTrayekBinding activityListTrayekBinding;
    TrayekAdapter trayekAdapter;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityListTrayekBinding = ActivityListTrayekBinding.inflate(getLayoutInflater());
        setContentView(activityListTrayekBinding.getRoot());

        pd = new ProgressDialog(ListTrayek.this);

        setSupportActionBar(activityListTrayekBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        pd.setMessage("Menampilkan List Trayek...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        loadTrayek();
    }

    private void loadTrayek() {
        new JsonTaskListTrayek().execute("http://167.172.143.101/api/GetBaseRoute");
        TrayekAdapter.keterangan = "dari activity";
    }

    private class JsonTaskListTrayek extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            List< ListTrayekModel > mapModels = new ArrayList<>();
            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("trayek");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    int id = jo.getInt("id");
                    String trayek = jo.getString("trayek");
                    String origin = jo.getString("origin");
                    String destination = jo.getString("destination");

                    Log.d("TAG","trayek = "+jo.getString("trayek"));
                    mapModels.add(new ListTrayekModel(id, trayek, origin, destination));
                }
                trayekAdapter = new TrayekAdapter(mapModels, ListTrayek.this);
                activityListTrayekBinding.rvTrayek.setLayoutManager(new LinearLayoutManager(ListTrayek.this));
                activityListTrayekBinding.rvTrayek.setAdapter(trayekAdapter);
                pd.dismiss();
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }
}