package com.projek.jogjaloop.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.Model.RuteModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.RuteAdapter;
import com.projek.jogjaloop.ViewModel.TrayekAdapter;
import com.projek.jogjaloop.databinding.ActivityFilterPolylineBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.projek.jogjaloop.View.DetailListTrayek.id_trayek;

public class FilterPolyline extends AppCompatActivity implements OnMapReadyCallback, RuteAdapter.ItemAdapterCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;
    private View mapView;
    Polyline polyline;
    ProgressDialog pd;

    List< RuteModel > ruteModelList = new ArrayList<>();
    RuteAdapter ruteAdapter;

    int id_trayek_rute_recycle;

    ActivityFilterPolylineBinding activityFilterPolylineBinding;

    public static String trayek_1a = "";
    public static String trayek_1b = "";
    public static String trayek_2a = "";
    public static String trayek_2b = "";
    public static String trayek_3a = "";
    public static String trayek_3b = "";
    public static String trayek_4a = "";
    public static String trayek_4b = "";
    public static String trayek_5a = "";
    public static String trayek_5b = "";
    public static String trayek_6a = "";
    public static String trayek_6b = "";
    public static String trayek_7 = "";
    public static String trayek_8 = "";
    public static String trayek_9 = "";
    public static String trayek_10 = "";
    public static String trayek_11 = "";
    public static String trayek_12 = "";
    public static String trayek_13 = "";
    public static String trayek_14 = "";

    public static int jumlah_pilih = 0;
    public static int id_trayek_pilih_polyline;

    public static int id_trayek_1a, id_trayek_1b, id_trayek_2a, id_trayek_2b, id_trayek_3a, id_trayek_3b, id_trayek_4a,
            id_trayek_4b, id_trayek_5a, id_trayek_5b, id_trayek_6a, id_trayek_6b, id_trayek_7, id_trayek_8, id_trayek_9,
            id_trayek_10, id_trayek_11, id_trayek_12, id_trayek_13, id_trayek_14;

    public static int drawable_trayek_1a, drawable_trayek_1b, drawable_trayek_2a, drawable_trayek_2b, drawable_trayek_3a, drawable_trayek_3b, drawable_trayek_4a,
            drawable_trayek_4b, drawable_trayek_5a, drawable_trayek_5b, drawable_trayek_6a, drawable_trayek_6b, drawable_trayek_7, drawable_trayek_8, drawable_trayek_9,
            drawable_trayek_10, drawable_trayek_11, drawable_trayek_12, drawable_trayek_13, drawable_trayek_14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityFilterPolylineBinding = ActivityFilterPolylineBinding.inflate(getLayoutInflater());
        setContentView(activityFilterPolylineBinding.getRoot());

        setSupportActionBar(activityFilterPolylineBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        activityFilterPolylineBinding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottom(view);
            }
        });

        pd = new ProgressDialog(FilterPolyline.this);
        pd.setMessage("Menampilkan arah...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        if (jumlah_pilih == 1){
            activityFilterPolylineBinding.tvKeterangan.setVisibility(View.GONE);
            activityFilterPolylineBinding.button.setVisibility(View.GONE);

            activityFilterPolylineBinding.rvRute.setVisibility(View.VISIBLE);
            activityFilterPolylineBinding.tvGaris.setVisibility(View.VISIBLE);
            activityFilterPolylineBinding.tvRuteTrayek.setVisibility(View.VISIBLE);
            activityFilterPolylineBinding.tvNamaTrayek.setVisibility(View.VISIBLE);
            activityFilterPolylineBinding.cardView.setVisibility(View.VISIBLE);

            pd.show();;
            pd.setMessage("Harap tunggu...");

            getMaps();
            getMaps_poyline();
            getRoute_trayek();

        }else {
            getMaps();
            getRoute();
        }
    }

    private void getRoute_trayek() {
        if (trayek_1a.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_1a);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 1A");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Prambanan - Ringroad Selatan");
        }

        if (trayek_1b.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_1b);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 1B");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Bandara Adisutjipto - JI. Laksda Adisutjipto");
        }

        if (trayek_2a.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_2a);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 2A");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Jombor - Terminal Condongcatur");
        }

        if (trayek_2b.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_2b);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 2B");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Jombor - Jl. AM.Sangaji");
        }

        if (trayek_3a.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_3a);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 3A");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Imogiri Timur");
        }

        if (trayek_3b.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_3b);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 3B");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Imogiri Timur");
        }

        if (trayek_4a.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_1a);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 4A");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Suroto");
        }

        if (trayek_4b.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_4b);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 4B");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Pramuka");
        }

        if (trayek_5a.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_5a);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 5A");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Jombor - Jl. Ringroad Utara");
        }

        if (trayek_5b.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_5b);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 5B");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Jombor - Jl. Magelang");
        }

        if (trayek_6a.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_6a);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 6A");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Ngabean - Jl. KH. Wahid Hasyim");
        }

        if (trayek_6b.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_6b);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 6B");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Ngabean - Jl.KH Wahid Hasyim");
        }

        if (trayek_7.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_7);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 7");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Imogiri Timur");
        }

        if (trayek_8.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id="+ id_trayek_8);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 8");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Jombor - Ringroad utara");
        }

        if (trayek_9.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_9);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 9");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Ringroad Selatan");
        }

        if (trayek_10.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_10);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 10");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl.Imogiri Timur");
        }

        if (trayek_11.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_11);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 11");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Terminal Condong Catur");
        }

        if (trayek_12.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_12);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 12");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Pasar Pakem A - HALTE TJ BANDARA ADI SUCIPTO");
        }

        if (trayek_13.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_13);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 13");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Bus Condong Catur - Terminal Pakem");
        }

        if (trayek_14.equals("pilih")) {
            new JsonTaskRoutesRutePilih().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_14);
            activityFilterPolylineBinding.tvNamaTrayek.setText("Trayek 14");
            activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
            activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Pusat Kuliner Belut Godean - Terminal Ngabean");
        }
    }

    private void getMaps_poyline() {
        new JsonTaskRoutes_1a().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_pilih_polyline);
    }

    private void getRoute() {
        if (trayek_1a.equals("pilih")) {
            new JsonTaskRoutes_1a().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_1a);
        }

        if (trayek_1b.equals("pilih")) {
            new JsonTaskRoutes_1b().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_1b);
        }

        if (trayek_2a.equals("pilih")) {
            new JsonTaskRoutes_2a().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_2a);
        }

        if (trayek_2b.equals("pilih")) {
            new JsonTaskRoutes_2b().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_2b);
        }

        if (trayek_3a.equals("pilih")) {
            new JsonTaskRoutes_3a().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_3a);
        }

        if (trayek_3b.equals("pilih")) {
            new JsonTaskRoutes_3b().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_3b);
        }

        if (trayek_4a.equals("pilih")) {
            new JsonTaskRoutes_4a().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_1a);
        }

        if (trayek_4b.equals("pilih")) {
            new JsonTaskRoutes_4b().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_4b);
        }

        if (trayek_5a.equals("pilih")) {
            new JsonTaskRoutes_5a().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_5a);
        }

        if (trayek_5b.equals("pilih")) {
            new JsonTaskRoutes_5b().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_5b);
        }

        if (trayek_6a.equals("pilih")) {
            new JsonTaskRoutes_6a().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_6a);
        }

        if (trayek_6b.equals("pilih")) {
            new JsonTaskRoutes_6b().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_6b);
        }

        if (trayek_7.equals("pilih")) {
            new JsonTaskRoutes_7().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_7);
        }

        if (trayek_8.equals("pilih")) {
            new JsonTaskRoutes_8().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_8);
        }

        if (trayek_9.equals("pilih")) {
            new JsonTaskRoutes_9().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_9);
        }

        if (trayek_10.equals("pilih")) {
            new JsonTaskRoutes_10().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_10);
        }

        if (trayek_11.equals("pilih")) {
            new JsonTaskRoutes_11().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_11);
        }

        if (trayek_12.equals("pilih")) {
            new JsonTaskRoutes_12().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_12);
        }

        if (trayek_13.equals("pilih")) {
            new JsonTaskRoutes_13().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_13);
        }

        if (trayek_14.equals("pilih")) {
            new JsonTaskRoutes_14().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_14);
        }
    }

    private void bottom(View view) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(view.getContext(), R.style.BottomSheetDialogTheme);
        final View view1 = LayoutInflater.from(view.getContext()).inflate(R.layout.bottom_sheet, (LinearLayout) view.findViewById(R.id.linearLayout));

        if (trayek_1a.equals("pilih")) {
            view1.findViewById(R.id.rd_1a).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_1a).setVisibility(View.GONE);
        }

        if (trayek_1b.equals("pilih")) {
            view1.findViewById(R.id.rd_1b).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_1b).setVisibility(View.GONE);
        }

        if (trayek_2a.equals("pilih")) {
            view1.findViewById(R.id.rd_2a).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_2a).setVisibility(View.GONE);
        }

        if (trayek_2b.equals("pilih")) {
            view1.findViewById(R.id.rd_2b).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_2b).setVisibility(View.GONE);
        }

        if (trayek_3a.equals("pilih")) {
            view1.findViewById(R.id.rd_3a).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_3a).setVisibility(View.GONE);
        }
        if (trayek_3b.equals("pilih")) {
            view1.findViewById(R.id.rd_3b).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_3b).setVisibility(View.GONE);
        }

        if (trayek_4a.equals("pilih")) {
            view1.findViewById(R.id.rd_4a).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_4a).setVisibility(View.GONE);
        }

        if (trayek_4b.equals("pilih")) {
            view1.findViewById(R.id.rd_4b).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_4b).setVisibility(View.GONE);
        }

        if (trayek_5a.equals("pilih")) {
            view1.findViewById(R.id.rd_5a).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_5a).setVisibility(View.GONE);
        }

        if (trayek_5b.equals("pilih")) {
            view1.findViewById(R.id.rd_5b).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_5b).setVisibility(View.GONE);
        }

        if (trayek_6a.equals("pilih")) {
            view1.findViewById(R.id.rd_6a).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_6a).setVisibility(View.GONE);
        }

        if (trayek_6b.equals("pilih")) {
            view1.findViewById(R.id.rd_6b).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_6b).setVisibility(View.GONE);
        }

        if (trayek_7.equals("pilih")) {
            view1.findViewById(R.id.rd_7).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_7).setVisibility(View.GONE);
        }

        if (trayek_8.equals("pilih")) {
            view1.findViewById(R.id.rd_8).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_8).setVisibility(View.GONE);
        }

        if (trayek_9.equals("pilih")) {
            view1.findViewById(R.id.rd_9).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_9).setVisibility(View.GONE);
        }

        if (trayek_10.equals("pilih")) {
            view1.findViewById(R.id.rd_10).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_10).setVisibility(View.GONE);
        }

        if (trayek_11.equals("pilih")) {
            view1.findViewById(R.id.rd_11).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_11).setVisibility(View.GONE);
        }

        if (trayek_12.equals("pilih")) {
            view1.findViewById(R.id.rd_12).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_12).setVisibility(View.GONE);
        }

        if (trayek_13.equals("pilih")) {
            view1.findViewById(R.id.rd_13).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_13).setVisibility(View.GONE);
        }

        if (trayek_14.equals("pilih")) {
            view1.findViewById(R.id.rd_14).setVisibility(View.VISIBLE);
        } else {
            view1.findViewById(R.id.rd_14).setVisibility(View.GONE);
        }

        view1.findViewById(R.id.btn_pilih).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RadioGroup radioGroup = view1.findViewById(R.id.radioGroup);
                RadioButton radioButton;

                int radioId = radioGroup.getCheckedRadioButtonId();
                radioButton = view1.findViewById(radioId);

                if (radioId == -1) {
                    bottomSheetDialog.dismiss();
                } else {

                    String nama_trayek = radioButton.getText().toString();
                    if (nama_trayek.equals("Trayek 1A")){
                        id_trayek_rute_recycle = 1;
                    }else if (nama_trayek.equals("Trayek 1B")){
                        id_trayek_rute_recycle = 2;
                    }else if (nama_trayek.equals("Trayek 2A")){
                        id_trayek_rute_recycle = 3;
                    }else if (nama_trayek.equals("Trayek 2B")){
                        id_trayek_rute_recycle = 4;
                    }else if (nama_trayek.equals("Trayek 3A")){
                        id_trayek_rute_recycle = 5;
                    }else if (nama_trayek.equals("Trayek 3B")){
                        id_trayek_rute_recycle = 6;
                    }else if (nama_trayek.equals("Trayek 4A")){
                        id_trayek_rute_recycle = 7;
                    }else if (nama_trayek.equals("Trayek 4B")){
                        id_trayek_rute_recycle = 8;
                    }else if (nama_trayek.equals("Trayek 5A")){
                        id_trayek_rute_recycle = 9;
                    }else if (nama_trayek.equals("Trayek 5B")){
                        id_trayek_rute_recycle = 10;
                    }else if (nama_trayek.equals("Trayek 6A")){
                        id_trayek_rute_recycle = 11;
                    }else if (nama_trayek.equals("Trayek 6B")){
                        id_trayek_rute_recycle = 12;
                    }else if (nama_trayek.equals("Trayek 7")){
                        id_trayek_rute_recycle = 13;
                    }else if (nama_trayek.equals("Trayek 8")){
                        id_trayek_rute_recycle = 14;
                    }else if (nama_trayek.equals("Trayek 9")){
                        id_trayek_rute_recycle = 15;
                    }else if (nama_trayek.equals("Trayek 10")){
                        id_trayek_rute_recycle = 16;
                    }else if (nama_trayek.equals("Trayek 11")){
                        id_trayek_rute_recycle = 17;
                    }else if (nama_trayek.equals("Trayek 12")){
                        id_trayek_rute_recycle = 18;
                    }else if (nama_trayek.equals("Trayek 13")){
                        id_trayek_rute_recycle = 19;
                    }else {
                        id_trayek_rute_recycle = 20;
                    }

                    activityFilterPolylineBinding.rvRute.setVisibility(View.VISIBLE);
                    activityFilterPolylineBinding.tvGaris.setVisibility(View.VISIBLE);
                    activityFilterPolylineBinding.tvRuteTrayek.setVisibility(View.VISIBLE);
                    activityFilterPolylineBinding.tvNamaTrayek.setVisibility(View.VISIBLE);
                    activityFilterPolylineBinding.cardView.setVisibility(View.VISIBLE);
                    activityFilterPolylineBinding.button2.setVisibility(View.VISIBLE);

                    activityFilterPolylineBinding.button.setVisibility(View.GONE);
                    activityFilterPolylineBinding.tvKeterangan.setVisibility(View.GONE);

                    activityFilterPolylineBinding.tvNamaTrayek.setText(nama_trayek);

                    if (nama_trayek.equals("Trayek 1A")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1a);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Prambanan - Ringroad Selatan");
                    }else if (nama_trayek.equals("Trayek 1B")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_1b);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Bandara Adisutjipto - JI. Laksda Adisutjipto");
                    }else if (nama_trayek.equals("Trayek 2A")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_2a);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Jombor - Terminal Condongcatur");
                    }else if (nama_trayek.equals("Trayek 2B")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_2b);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Jombor - Jl. AM.Sangaji");
                    }else if (nama_trayek.equals("Trayek 3A")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_3a);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Imogiri Timur");
                    }else if (nama_trayek.equals("Trayek 3B")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_3b);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Imogiri Timur");
                    }else if (nama_trayek.equals("Trayek 4A")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_4a);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Suroto");
                    }else if (nama_trayek.equals("Trayek 4B")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_4b);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Pramuka");
                    }else if (nama_trayek.equals("Trayek 5A")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_5a);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Jombor - Jl. Ringroad Utara");
                    }else if (nama_trayek.equals("Trayek 5B")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_5b);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Jombor - Jl. Magelang");
                    }else if (nama_trayek.equals("Trayek 6A")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_6a);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Ngabean - Jl. KH. Wahid Hasyim");
                    }else if (nama_trayek.equals("Trayek 6B")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_6b);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Ngabean - Jl.KH Wahid Hasyim");
                    }else if (nama_trayek.equals("Trayek 7")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_7);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl. Imogiri Timur");
                    }else if (nama_trayek.equals("Trayek 8")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_8);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Jombor - Ringroad utara");
                    }else if (nama_trayek.equals("Trayek 9")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_9);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Ringroad Selatan");
                    }else if (nama_trayek.equals("Trayek 10")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_10);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Jl.Imogiri Timur");
                    }else if (nama_trayek.equals("Trayek 11")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_11);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Giwangan - Terminal Condong Catur");
                    }else if (nama_trayek.equals("Trayek 12")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_12);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Pasar Pakem A - HALTE TJ BANDARA ADI SUCIPTO");
                    }else if (nama_trayek.equals("Trayek 13")){
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_13);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Terminal Bus Condong Catur - Terminal Pakem");
                    }else {
                        activityFilterPolylineBinding.cardView.setBackgroundResource(R.drawable.ic_color_14);
                        activityFilterPolylineBinding.tvRuteTrayek.setText("Halte Pusat Kuliner Belut Godean - Terminal Ngabean");
                    }

                    pd.show();;
                    pd.setMessage("Harap tunggu...");

                    if (ruteModelList.size() <= 0){

                    }else {
                        ruteModelList.clear();
                        ruteAdapter.notifyDataSetChanged();
                    }
                    new JsonTaskRoutesRute().execute("http://167.172.143.101/api/GetBaseRoute?id=" + id_trayek_rute_recycle);

                    activityFilterPolylineBinding.button2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            bottom(view);
                        }
                    });

                    bottomSheetDialog.dismiss();
                }
            }
        });

        bottomSheetDialog.setContentView(view1);
        bottomSheetDialog.show();
    }

    private void getMaps() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(FilterPolyline.this);
        Places.initialize(FilterPolyline.this, getString(R.string.api_key));
        placesClient = Places.createClient(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-34.364, 147.891), 6));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            trayek_1a = "";
            trayek_1b = "";
            trayek_2a = "";
            trayek_2b = "";
            trayek_3a = "";
            trayek_3b = "";
            trayek_4a = "";
            trayek_4b = "";
            trayek_5a = "";
            trayek_5b = "";
            trayek_6a = "";
            trayek_6b = "";
            trayek_7 = "";
            trayek_8 = "";
            trayek_9 = "";
            trayek_10 = "";
            trayek_11 = "";
            trayek_12 = "";
            trayek_13 = "";
            trayek_14 = "";

            startActivity(new Intent(getApplicationContext(), FilterTrayek.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class JsonTaskRoutes_1a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_1a().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }

        }
    }

    private class JsonTaskRoutesPolyline_1a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#ACDEE7"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_1b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_1b().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }

        }
    }

    private class JsonTaskRoutesPolyline_1b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#CCAACB"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_2a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_2a().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }

        }
    }

    private class JsonTaskRoutesPolyline_2a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#FEFEB4"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_2b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_2b().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }

        }
    }

    private class JsonTaskRoutesPolyline_2b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#FECFB5"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_3a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_3a().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }

        }
    }

    private class JsonTaskRoutesPolyline_3a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#F1B1C2"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_3b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_3b().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_3b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#C7DBDA"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_4a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_4a().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_4a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#FDE2E9"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_4b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_4b().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_4b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#ECD6E3"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_5a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_5a().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_5a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#FEAEA5"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_5b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#941487"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_5b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_5b().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_6a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_6a().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_6a extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#8FC9CA"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_6b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_6b().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_6b extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#96C2A9"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_7 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_7().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_7 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#168428"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_8 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_8().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_8 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#0096DD"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_9 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_9().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_9 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#E00F0F"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_10 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_10().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_10 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#FF7A00"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_11 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_11().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_11 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#6000FC"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_12 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_12().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_12 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#00FC47"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_13 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_13().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_13 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#000000"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutes_14 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");

                ArrayList<LatLng> lat_lon;
                lat_lon = new ArrayList<>();

                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    LatLng mw = new LatLng(rutes, rutess);
                    lat_lon.add(mw);

                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(rutes, rutess))
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_pin_color))
                            .title(users.getString("name")));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }

                for (int k=0; k<lat_lon.size() - 1; k++){
                    new JsonTaskRoutesPolyline_14().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_lon.get(k).latitude + "%2C" + lat_lon.get(k).longitude + "%3A" + lat_lon.get(k + 1).latitude + "%2C" + lat_lon.get(k + 1).longitude + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-7.805120, 110.374157), 11));

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesPolyline_14 extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptionss = new PolylineOptions();
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            double rutes = users.getDouble("latitude");
                            double rutess = users.getDouble("longitude");
                            polylineOptionss.color(Color.parseColor("#FC7900"));
                            polylineOptionss.width(10);
                            polylineOptionss.add(new LatLng(rutes, rutess));

                        }
                        polyline = mMap.addPolyline(polylineOptionss);
                        pd.dismiss();
                    }
                }
            } catch (Exception e) {

                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesRute extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    ruteModelList.add(new RuteModel(users.getString("name"),rutes,rutess));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }
                ruteAdapter = new RuteAdapter(ruteModelList, FilterPolyline.this);
                activityFilterPolylineBinding.rvRute.setLayoutManager(new LinearLayoutManager(FilterPolyline.this));
                activityFilterPolylineBinding.rvRute.setAdapter(ruteAdapter);
                pd.dismiss();
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }

    private class JsonTaskRoutesRutePilih extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject users = jsonArray.getJSONObject(k);
                    double rutes = users.getDouble("lat");
                    double rutess = users.getDouble("lon");

                    ruteModelList.add(new RuteModel(users.getString("name"),rutes,rutess));

                    Log.d("TAG", "Hasil latitude = " + rutes);
                    Log.d("TAG", "Hasil longtitude = " + rutess);
                }
                ruteAdapter = new RuteAdapter(ruteModelList, FilterPolyline.this);
                activityFilterPolylineBinding.rvRute.setLayoutManager(new LinearLayoutManager(FilterPolyline.this));
                activityFilterPolylineBinding.rvRute.setAdapter(ruteAdapter);
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }


    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        trayek_1a = "";
        trayek_1b = "";
        trayek_2a = "";
        trayek_2b = "";
        trayek_3a = "";
        trayek_3b = "";
        trayek_4a = "";
        trayek_4b = "";
        trayek_5a = "";
        trayek_5b = "";
        trayek_6a = "";
        trayek_6b = "";
        trayek_7 = "";
        trayek_8 = "";
        trayek_9 = "";
        trayek_10 = "";
        trayek_11 = "";
        trayek_12 = "";
        trayek_13 = "";
        trayek_14 = "";

        startActivity(new Intent(getApplicationContext(), FilterTrayek.class));
        finish();
    }
}