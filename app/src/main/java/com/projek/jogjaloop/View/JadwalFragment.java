package com.projek.jogjaloop.View;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.Compass;
import com.projek.jogjaloop.ViewModel.Constants;
import com.projek.jogjaloop.ViewModel.GPSTracker;
import com.projek.jogjaloop.ViewModel.TrayekAdapter;
import com.projek.jogjaloop.databinding.FragmentBagiBinding;
import com.projek.jogjaloop.databinding.FragmentJadwalBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.INVISIBLE;

public class JadwalFragment extends Fragment {

    private static final String TAG = JadwalFragment.class.getSimpleName();
    private FragmentJadwalBinding fragmentJadwalBinding;
    ProgressDialog pd;

    //untuk kompas kiblat
    private Compass compass;
    private ImageView qiblatIndicator;
    private ImageView imageDial;
    private float currentAzimuth;
    SharedPreferences prefs;
    GPSTracker gps;
    private final int RC_Permission = 1221;
    public static double lat_pengguna, lon_pengguna;

    String tanggal_saat_ini;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentJadwalBinding = FragmentJadwalBinding.inflate(inflater, container, false);
        return fragmentJadwalBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pd = new ProgressDialog(getContext());
        pd.setMessage("Menampilkan Jadwal Shalat...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);


        setUserChanges(getActivity().getIntent());
        prefs = getActivity().getSharedPreferences("", MODE_PRIVATE);
        gps = new GPSTracker(getContext());
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        qiblatIndicator = fragmentJadwalBinding.qiblaIndicator;
        imageDial = fragmentJadwalBinding.dial;
        Log.d("TAG", "lokasi= ");
        setupCompass();
        fetch_GPS();


        getTanggal();
        getJadwalShalat();

        fragmentJadwalBinding.ivForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dateInString = tanggal_saat_ini;  // Start date
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(sdf.parse(dateInString));
                    c.add(Calendar.DATE, 1);
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date resultdate = new Date(c.getTimeInMillis());
                    dateInString = sdf.format(resultdate);
                    Log.d("TAG","tanggal besok adalah: "+dateInString);
                    tanggal_saat_ini = dateInString;

                    String tanggal_view = tanggal_saat_ini;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date newDate = format.parse(tanggal_view);

                    format = new SimpleDateFormat("dd MMMM yyyy");
                    String date_format = format.format(newDate);
                    fragmentJadwalBinding.tvTanggal.setText(date_format);

                    pd.setMessage("Menampilkan Jadwal Shalat...");
                    pd.show();
                    pd.setCancelable(false);
                    pd.setCanceledOnTouchOutside(false);
                    new JsonTaskJadwalShalat().execute("https://api.pray.zone/v2/times/day.json?city=yogyakarta&date="+dateInString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        fragmentJadwalBinding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dateInString = tanggal_saat_ini;  // Start date
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(sdf.parse(dateInString));
                    c.add(Calendar.DATE, -1);
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date resultdate = new Date(c.getTimeInMillis());
                    dateInString = sdf.format(resultdate);
                    Log.d("TAG","tanggal sebelum adalah: "+dateInString);
                    tanggal_saat_ini = dateInString;

                    String tanggal_view = tanggal_saat_ini;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date newDate = format.parse(tanggal_view);

                    format = new SimpleDateFormat("dd MMMM yyyy");
                    String date_format = format.format(newDate);
                    fragmentJadwalBinding.tvTanggal.setText(date_format);

                    pd.setMessage("Menampilkan Jadwal Shalat...");
                    pd.show();
                    pd.setCancelable(false);
                    pd.setCanceledOnTouchOutside(false);
                    new JsonTaskJadwalShalat().execute("https://api.pray.zone/v2/times/day.json?city=yogyakarta&date="+dateInString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getTanggal() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat dateFormat_convert = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date());
        tanggal_saat_ini = dateFormat_convert.format(new Date());
        String tanggal = java.text.DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());
        fragmentJadwalBinding.tvTanggal.setText(date);
        fragmentJadwalBinding.tvJamSaatIni.setText(tanggal);
    }

    private void getJadwalShalat() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date());
        new JsonTaskJadwalShalat().execute("https://api.pray.zone/v2/times/day.json?city=yogyakarta&date="+date);
    }

    private class JsonTaskJadwalShalat extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONObject jObject = jsonObject.getJSONObject("results");
                JSONArray jsonArray = jObject.getJSONArray("datetime");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONObject summary = jo.getJSONObject("times");

                    fragmentJadwalBinding.tvJamShubuh.setText(summary.getString("Fajr"));
                    fragmentJadwalBinding.tvJamDzuhur.setText(summary.getString("Dhuhr"));
                    fragmentJadwalBinding.tvJamAshar.setText(summary.getString("Asr"));
                    fragmentJadwalBinding.tvJamMarghrib.setText(summary.getString("Maghrib"));
                    fragmentJadwalBinding.tvJamIsya.setText(summary.getString("Isha"));
                    fragmentJadwalBinding.tvImsak.setText(summary.getString("Imsak"));
                }

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }

            pd.dismiss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TAG", "start compass");
        if (compass != null) {
            compass.start(getActivity().getApplicationContext());
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (compass != null) {
            compass.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (compass != null) {
            compass.start(getActivity().getApplicationContext());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG", "stop compass");
        if (compass != null) {
            compass.stop();
        }
        if (gps != null) {
            gps.stopUsingGPS();
            gps = null;
        }
    }

    private void setUserChanges(Intent intent) {
        try {

            // Dial
            fragmentJadwalBinding.dial.setImageResource(
                    (intent.getExtras() != null &&
                            intent.getExtras().containsKey(Constants.DRAWABLE_DIAL)) ?
                            intent.getExtras().getInt(Constants.DRAWABLE_DIAL) : R.drawable.compass);

            // Qibla Indicator
            fragmentJadwalBinding.qiblaIndicator.setImageResource(
                    (intent.getExtras() != null &&
                            intent.getExtras().containsKey(Constants.DRAWABLE_QIBLA)) ?
                            intent.getExtras().getInt(Constants.DRAWABLE_QIBLA) : R.drawable.qibla);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupCompass() {
        Boolean permission_granted = GetBoolean("permission_granted");
        if (permission_granted) {
            getBearing();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        RC_Permission);
            } else {
                fetch_GPS();
            }
        }


        compass = new Compass(getActivity().getApplicationContext());
        Compass.CompassListener cl = new Compass.CompassListener() {

            @Override
            public void onNewAzimuth(float azimuth) {
                // adjustArrow(azimuth);
                adjustGambarDial(azimuth);
                adjustArrowQiblat(azimuth);
            }
        };
        compass.setListener(cl);
    }


    public void adjustGambarDial(float azimuth) {
        // Log.d(TAG, "will set rotation from " + currentAzimuth + " to "                + azimuth);

        Animation an = new RotateAnimation(-currentAzimuth, -azimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        currentAzimuth = (azimuth);
        an.setDuration(500);
        an.setRepeatCount(0);
        an.setFillAfter(true);
        imageDial.startAnimation(an);
    }

    public void adjustArrowQiblat(float azimuth) {
        //Log.d(TAG, "will set rotation from " + currentAzimuth + " to "                + azimuth);

        float kiblat_derajat = GetFloat("kiblat_derajat");
        Animation an = new RotateAnimation(-(currentAzimuth) + kiblat_derajat, -azimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        currentAzimuth = (azimuth);
        an.setDuration(500);
        an.setRepeatCount(0);
        an.setFillAfter(true);
        qiblatIndicator.startAnimation(an);
        if (kiblat_derajat > 0) {
            qiblatIndicator.setVisibility(View.VISIBLE);
        } else {
            qiblatIndicator.setVisibility(INVISIBLE);
            qiblatIndicator.setVisibility(View.GONE);
        }
    }

    @SuppressLint("MissingPermission")
    public void getBearing() {
        // Get the location manager

        float kaabaDegs = GetFloat("kiblat_derajat");
        if (kaabaDegs > 0.0001) {
            String strYourLocation;
            if(gps.getLocation() != null){
                strYourLocation = getResources().getString(R.string.your_location)
                        + " " + gps.getLocation().getLatitude() + ", " + gps.getLocation().getLongitude();
            }
            else{
                strYourLocation = getResources().getString(R.string.unable_to_get_your_location);
                String strKaabaDirection = String.format(Locale.ENGLISH, "%.0f", kaabaDegs)
                        + " " + getResources().getString(R.string.degree) + " " + getDirectionString(kaabaDegs);
                Log.d("TAG","lokasi lokasi");
                qiblatIndicator.setVisibility(View.VISIBLE);
            }
        } else {
            fetch_GPS();
        }
    }

    private String getDirectionString(float azimuthDegrees) {
        String where = "NW";

        if (azimuthDegrees >= 350 || azimuthDegrees <= 10)
            where = "N";
        if (azimuthDegrees < 350 && azimuthDegrees > 280)
            where = "NW";
        if (azimuthDegrees <= 280 && azimuthDegrees > 260)
            where = "W";
        if (azimuthDegrees <= 260 && azimuthDegrees > 190)
            where = "SW";
        if (azimuthDegrees <= 190 && azimuthDegrees > 170)
            where = "S";
        if (azimuthDegrees <= 170 && azimuthDegrees > 100)
            where = "SE";
        if (azimuthDegrees <= 100 && azimuthDegrees > 80)
            where = "E";
        if (azimuthDegrees <= 80 && azimuthDegrees > 10)
            where = "NE";

        return where;
    }

    public void SaveBoolean(String Judul, Boolean bbb) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(Judul, bbb);
        edit.apply();
    }

    public Boolean GetBoolean(String Judul) {
        return prefs.getBoolean(Judul, false);
    }

    public void SaveFloat(String Judul, Float bbb) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putFloat(Judul, bbb);
        edit.apply();
    }

    public Float GetFloat(String Judul) {
        return prefs.getFloat(Judul, 0);
    }


    public void fetch_GPS() {
        Log.d("TAG", "GPS is on");
        double result;
        gps = new GPSTracker(getActivity().getApplicationContext());
        if (lat_pengguna != 0.0) {
            double myLat = lat_pengguna;
            double myLng = lon_pengguna;
            // \n is for new line
            String strYourLocation = getResources().getString(R.string.your_location)
                    + " " + myLat + ", " + myLng;
            Log.d("TAG", "lokasi= "+strYourLocation);
//            tvYourLocation.setText(strYourLocation);
            //Toast.makeText(getApplicationContext(), "Lokasi anda: - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            Log.e("TAG", "GPS is on");
            if (myLat < 0.001 && myLng < 0.001) {
                // qiblatIndicator.isShown(false);
                qiblatIndicator.setVisibility(INVISIBLE);
                qiblatIndicator.setVisibility(View.GONE);
            } else {
                double kaabaLng = 39.826206; // ka'bah Position https://www.latlong.net/place/kaaba-mecca-saudi-arabia-12639.html
                double kaabaLat = Math.toRadians(21.422487); // ka'bah Position https://www.latlong.net/place/kaaba-mecca-saudi-arabia-12639.html
                double myLatRad = Math.toRadians(myLat);
                double longDiff = Math.toRadians(kaabaLng - myLng);
                double y = Math.sin(longDiff) * Math.cos(kaabaLat);
                double x = Math.cos(myLatRad) * Math.sin(kaabaLat) - Math.sin(myLatRad) * Math.cos(kaabaLat) * Math.cos(longDiff);
                result = (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
                SaveFloat("kiblat_derajat", (float) result);
                String strKaabaDirection = String.format(Locale.ENGLISH, "%.0f", (float) result)
                        + " " + getResources().getString(R.string.degree) + " " + getDirectionString((float) result);
//                tvAngle.setText(strKaabaDirection);
                qiblatIndicator.setVisibility(View.VISIBLE);
            }
            //  Toast.makeText(getApplicationContext(), "lat_saya: "+lat_saya + "\nlon_saya: "+lon_saya, Toast.LENGTH_LONG).show();
        } else {
            gps.showSettingsAlert();

            // qiblatIndicator.isShown(false);
            qiblatIndicator.setVisibility(INVISIBLE);
            qiblatIndicator.setVisibility(View.GONE);
        }
    }
}