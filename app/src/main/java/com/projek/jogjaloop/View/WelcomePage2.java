package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.projek.jogjaloop.R;
import com.projek.jogjaloop.databinding.ActivityWelcomePage2Binding;

public class WelcomePage2 extends AppCompatActivity {

    ActivityWelcomePage2Binding activityWelcomePage2Binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityWelcomePage2Binding = ActivityWelcomePage2Binding.inflate(getLayoutInflater());
        setContentView(activityWelcomePage2Binding.getRoot());

        getSupportActionBar().hide();

//        activityWelcomePage2Binding.btnMasuk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), BottomNavigationActivity.class));
//                finish();
//            }
//        });
    }
}