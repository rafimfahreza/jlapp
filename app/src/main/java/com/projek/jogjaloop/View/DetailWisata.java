package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.snackbar.Snackbar;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.databinding.ActivityDetailWisataBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DetailWisata extends AppCompatActivity implements OnMapReadyCallback {

    ActivityDetailWisataBinding activityDetailWisataBinding;

    private CheckBox cb_1a, cb_1b, cb_2a, cb_2b, cb_3a, cb_3b, cb_4a, cb_4b, cb_5a, cb_5b, cb_6a, cb_6b, cb_7, cb_8, cb_9, cb_10, cb_11, cb_12, cb_13, cb_14;
    Integer jumlah_pilih = 0;

    public static String nama_lokasi, nama_jalan;
    public static double lat_destinasi, lon_destinasi, lat_pengguna, long_pengguna;
    ProgressDialog pd;

    private GoogleMap mMap, maps, map_line;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;
    private View mapView;
    private Polyline polyline;
    private Polyline polyline_bis;
    Integer jumlah_cari = 0;
    Integer jumlah_klik = 0;
    Marker markerOptions;
    Marker markerOptions_bis;

    String departureTime;
    String arrivalTime;
    Integer travelTimeInSeconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityDetailWisataBinding = ActivityDetailWisataBinding.inflate(getLayoutInflater());
        setContentView(activityDetailWisataBinding.getRoot());

        setSupportActionBar(activityDetailWisataBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        pd = new ProgressDialog(DetailWisata.this);
        pd.setMessage("Menampilkan arah...");
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        getDetail();
        getMaps();
        getCheckBoxTrayek();
    }

    private void getCheckBoxTrayek() {
        cb_1a = activityDetailWisataBinding.cb1a;
        cb_1b = activityDetailWisataBinding.cb1b;
        cb_2a = activityDetailWisataBinding.cb2a;
        cb_2b = activityDetailWisataBinding.cb2b;
        cb_3a = activityDetailWisataBinding.cb3a;
        cb_3b = activityDetailWisataBinding.cb3b;
        cb_4a = activityDetailWisataBinding.cb4a;
        cb_4b = activityDetailWisataBinding.cb4b;
        cb_5a = activityDetailWisataBinding.cb5a;
        cb_5b = activityDetailWisataBinding.cb5b;
        cb_6a = activityDetailWisataBinding.cb6a;
        cb_6b = activityDetailWisataBinding.cb6b;
        cb_7 = activityDetailWisataBinding.cb7;
        cb_8 = activityDetailWisataBinding.cb8;
        cb_9 = activityDetailWisataBinding.cb9;
        cb_10 = activityDetailWisataBinding.cb10;
        cb_11 = activityDetailWisataBinding.cb11;
        cb_12 = activityDetailWisataBinding.cb12;
        cb_13 = activityDetailWisataBinding.cb13;
        cb_14 = activityDetailWisataBinding.cb14;

        activityDetailWisataBinding.btnPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cb_1a.isChecked() && !cb_1b.isChecked() && !cb_2a.isChecked() && !cb_2b.isChecked() && !cb_3a.isChecked()
                        && !cb_3b.isChecked() && !cb_4a.isChecked() && !cb_4b.isChecked() && !cb_5a.isChecked() && !cb_5b.isChecked()
                        && !cb_6a.isChecked() && !cb_6b.isChecked() && !cb_7.isChecked() && !cb_8.isChecked() && !cb_9.isChecked()
                        && !cb_10.isChecked() && !cb_11.isChecked() && !cb_12.isChecked() && !cb_13.isChecked() && !cb_14.isChecked())
                {
                    Snackbar snackbar = Snackbar.make(activityDetailWisataBinding.layout, "Belum ada trayek yg dipilih.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else {

                    if (cb_1a.isChecked()){
                        FilterPolylineDetailWisata.trayek_1a = "pilih";
                        FilterPolylineDetailWisata.id_trayek_1a = 1;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 1;
                        FilterPolylineDetailWisata.drawable_trayek_1a = R.drawable.ic_color_1a;

                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_1b.isChecked()){
                        FilterPolylineDetailWisata.trayek_1b = "pilih";
                        FilterPolylineDetailWisata.id_trayek_1b = 2;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 2;
                        FilterPolylineDetailWisata.drawable_trayek_1b = R.drawable.ic_color_1b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_2a.isChecked()){
                        FilterPolylineDetailWisata.trayek_2a = "pilih";
                        FilterPolylineDetailWisata.id_trayek_2a = 3;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 3;
                        FilterPolylineDetailWisata.drawable_trayek_2a = R.drawable.ic_color_2a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_2b.isChecked()){
                        FilterPolylineDetailWisata.trayek_2b = "pilih";
                        FilterPolylineDetailWisata.id_trayek_2b = 4;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 4;
                        FilterPolylineDetailWisata.drawable_trayek_2b = R.drawable.ic_color_2b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_3a.isChecked()){
                        FilterPolylineDetailWisata.trayek_3a = "pilih";
                        FilterPolylineDetailWisata.id_trayek_3a = 5;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 5;
                        FilterPolylineDetailWisata.drawable_trayek_3a = R.drawable.ic_color_3a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_3b.isChecked()){
                        FilterPolylineDetailWisata.trayek_3b = "pilih";
                        FilterPolylineDetailWisata.id_trayek_3b = 6;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 6;
                        FilterPolylineDetailWisata.drawable_trayek_3b = R.drawable.ic_color_3b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_4a.isChecked()){
                        FilterPolylineDetailWisata.trayek_4a = "pilih";
                        FilterPolylineDetailWisata.id_trayek_4a = 7;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 7;
                        FilterPolylineDetailWisata.drawable_trayek_4a = R.drawable.ic_color_4a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_4b.isChecked()){
                        FilterPolylineDetailWisata.trayek_4b = "pilih";
                        FilterPolylineDetailWisata.id_trayek_4b = 8;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 8;
                        FilterPolylineDetailWisata.drawable_trayek_4b = R.drawable.ic_color_4b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_5a.isChecked()){
                        FilterPolylineDetailWisata.trayek_5a = "pilih";
                        FilterPolylineDetailWisata.id_trayek_5a = 9;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 9;
                        FilterPolylineDetailWisata.drawable_trayek_5a = R.drawable.ic_color_5b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_5b.isChecked()){
                        FilterPolylineDetailWisata.trayek_5b = "pilih";
                        FilterPolylineDetailWisata.id_trayek_5b = 10;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 10;
                        FilterPolylineDetailWisata.drawable_trayek_5b = R.drawable.ic_color_5b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_6a.isChecked()){
                        FilterPolylineDetailWisata.trayek_6a = "pilih";
                        FilterPolylineDetailWisata.id_trayek_6a = 11;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 11;
                        FilterPolyline.drawable_trayek_6a = R.drawable.ic_color_6a;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_6b.isChecked()){
                        FilterPolylineDetailWisata.trayek_6b = "pilih";
                        FilterPolylineDetailWisata.id_trayek_6b = 12;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 12;
                        FilterPolylineDetailWisata.drawable_trayek_6b = R.drawable.ic_color_6b;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_7.isChecked()){
                        FilterPolylineDetailWisata.trayek_7 = "pilih";
                        FilterPolylineDetailWisata.id_trayek_7 = 13;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 13;
                        FilterPolylineDetailWisata.drawable_trayek_7 = R.drawable.ic_color_7;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_8.isChecked()){
                        FilterPolylineDetailWisata.trayek_8 = "pilih";
                        FilterPolylineDetailWisata.id_trayek_8 = 14;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 14;
                        FilterPolylineDetailWisata.drawable_trayek_8 = R.drawable.ic_color_8;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_9.isChecked()){
                        FilterPolylineDetailWisata.trayek_9 = "pilih";
                        FilterPolylineDetailWisata.id_trayek_9 = 15;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 15;
                        FilterPolylineDetailWisata.drawable_trayek_9 = R.drawable.ic_color_9;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_10.isChecked()){
                        FilterPolylineDetailWisata.trayek_10 = "pilih";
                        FilterPolylineDetailWisata.id_trayek_10 = 16;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 16;
                        FilterPolylineDetailWisata.drawable_trayek_10 = R.drawable.ic_color_10;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_11.isChecked()){
                        FilterPolylineDetailWisata.trayek_11 = "pilih";
                        FilterPolylineDetailWisata.id_trayek_11 = 17;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 17;
                        FilterPolylineDetailWisata.drawable_trayek_11 = R.drawable.ic_color_11;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_12.isChecked()){
                        FilterPolylineDetailWisata.trayek_12 = "pilih";
                        FilterPolylineDetailWisata.id_trayek_12 = 18;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 18;
                        FilterPolylineDetailWisata.drawable_trayek_12 = R.drawable.ic_color_12;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_13.isChecked()){
                        FilterPolylineDetailWisata.trayek_13 = "pilih";
                        FilterPolylineDetailWisata.id_trayek_13 = 19;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 19;
                        FilterPolylineDetailWisata.drawable_trayek_13 = R.drawable.ic_color_13;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    if (cb_14.isChecked()){
                        FilterPolylineDetailWisata.trayek_14 = "pilih";
                        FilterPolylineDetailWisata.id_trayek_14 = 20;
                        FilterPolylineDetailWisata.id_trayek_pilih_polyline = 20;
                        FilterPolylineDetailWisata.drawable_trayek_14 = R.drawable.ic_color_14;
                        jumlah_pilih = jumlah_pilih + 1;
                    }

                    FilterPolylineDetailWisata.nama_lokasi = nama_lokasi;
                    FilterPolylineDetailWisata.lat_search = lat_destinasi;
                    FilterPolylineDetailWisata.long_search = lon_destinasi;
                    FilterPolylineDetailWisata.jumlah_pilih = jumlah_pilih;
                    FilterPolylineDetailWisata.lat_pengguna = lat_pengguna;
                    FilterPolylineDetailWisata.long_pengguna = long_pengguna;
                    startActivity(new Intent(getApplicationContext(), FilterPolylineDetailWisata.class));
                    finish();
                }
            }
        });
    }

    private void getDetail() {
        activityDetailWisataBinding.tvJalan.setText(nama_jalan);
        activityDetailWisataBinding.tvNamaWisata.setText(nama_lokasi);

        new JsonTaskDetailEstimasi().execute("https://api.tomtom.com/routing/1/calculateRoute/"+lat_pengguna+"%2C"+long_pengguna+"%3A"+lat_destinasi+"%2C"+lon_destinasi+"/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");

        activityDetailWisataBinding.ivZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StreetView.lat = lat_destinasi;
                StreetView.lon = lon_destinasi;
                StreetView.keterangan = "wisata";
                startActivity(new Intent(getApplicationContext(), StreetView.class));
                finish();
            }
        });
    }

    private void getMaps() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(DetailWisata.this);
        Places.initialize(DetailWisata.this, getString(R.string.api_key));
        placesClient = Places.createClient(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat_destinasi, lon_destinasi), 12));
        LatLng m1 = new LatLng(lat_destinasi, lon_destinasi);
        markerOptions = mMap.addMarker(new MarkerOptions()
                .position(m1)
                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.pin_dituju))
                .title(nama_lokasi));

        new JsonTaskRoutesDestinasi().execute("https://api.tomtom.com/routing/1/calculateRoute/" + lat_pengguna + "%2C" + long_pengguna + "%3A" + lat_destinasi + "%2C" + lon_destinasi + "/json?avoid=unpavedRoads&key=mOaQKeK1eAlKax1ajbnGj8gZDm5vCFHk");
    }

    private class JsonTaskRoutesDestinasi extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONArray user = jo.getJSONArray("legs");
                    for (int j = 0; j < user.length(); j++) {
                        JSONObject routes = user.getJSONObject(j);
                        JSONArray rute = routes.getJSONArray("points");

                        PolylineOptions polylineOptions = new PolylineOptions();
                        double rutes = 0.0;
                        double rutess = 0.0;
                        for (int k = 0; k < rute.length(); k++) {
                            JSONObject users = rute.getJSONObject(k);
                            rutes = users.getDouble("latitude");
                            rutess = users.getDouble("longitude");
                            polylineOptions.color(Color.parseColor("#4bc5d2"));
                            polylineOptions.width(10);
                            polylineOptions.add(new LatLng(rutes, rutess));

                            Log.d("TAG", "Hasil latitude = " + rutes);
                            Log.d("TAG", "Hasil longtitude = " + rutess);
                        }
                        polyline = mMap.addPolyline(polylineOptions);
                    }
                }

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
            pd.dismiss();
        }
    }

    private class JsonTaskDetailEstimasi extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("routes");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    JSONObject summary = jo.getJSONObject("summary");
                    departureTime = summary.getString("departureTime").substring(11, 16);
                    arrivalTime = summary.getString("arrivalTime").substring(11, 16);
                    travelTimeInSeconds = summary.getInt("travelTimeInSeconds");
                }

            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
            Integer waktu = travelTimeInSeconds / 60;
            activityDetailWisataBinding.estimasiPerjalanan.tvEstimasiWaktu.setText(""+waktu);
            activityDetailWisataBinding.estimasiPerjalanan.tvJamDatang.setText(departureTime);
            activityDetailWisataBinding.estimasiPerjalanan.tvSampai.setText(arrivalTime);
            activityDetailWisataBinding.estimasiPerjalanan.tvLokasiTujuan.setText(nama_lokasi);
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}