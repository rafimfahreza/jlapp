package com.projek.jogjaloop.View;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.projek.jogjaloop.R;
import com.projek.jogjaloop.databinding.FragmentBagiBinding;
import com.projek.jogjaloop.databinding.FragmentRuteBinding;
import com.synnapps.carouselview.ImageListener;

public class BagiFragment extends Fragment {

    FragmentBagiBinding fragmentBagiBinding;
    private int[] gambar = new int[]{R.drawable.carousel_14, R.drawable.carousel_12, R.drawable.carousel_13};
    private String[] judul = new String[]{"Carousel 1", "Carouse 2", "Carousel 3"};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentBagiBinding = FragmentBagiBinding.inflate(inflater, container, false);
        return fragmentBagiBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentBagiBinding.carousel.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(gambar[position]);
            }
        });
        fragmentBagiBinding.carousel.setPageCount(gambar.length);
    }
}