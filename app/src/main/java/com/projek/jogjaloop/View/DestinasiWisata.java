package com.projek.jogjaloop.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.projek.jogjaloop.Model.DestinasiModel;
import com.projek.jogjaloop.Model.ListTrayekModel;
import com.projek.jogjaloop.Model.RuteModel;
import com.projek.jogjaloop.R;
import com.projek.jogjaloop.ViewModel.DestinasiAdapter;
import com.projek.jogjaloop.ViewModel.RuteAdapter;
import com.projek.jogjaloop.ViewModel.TrayekAdapter;
import com.projek.jogjaloop.databinding.ActivityDestinasiWisataBinding;
import com.projek.jogjaloop.databinding.ActivityPdfRuteBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DestinasiWisata extends AppCompatActivity implements DestinasiAdapter.ItemAdapterCallback{

    ActivityDestinasiWisataBinding activityDestinasiWisataBinding;

    List< DestinasiModel > destinasiModelList = new ArrayList<>();
    DestinasiAdapter destinasiAdapter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityDestinasiWisataBinding = ActivityDestinasiWisataBinding.inflate(getLayoutInflater());
        setContentView(activityDestinasiWisataBinding.getRoot());

        setSupportActionBar(activityDestinasiWisataBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        progressDialog = new ProgressDialog(DestinasiWisata.this);
        progressDialog.setMessage("Menampilkan List Wisata...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        loadDestinasi();
    }

    private void loadDestinasi() {
        new JsonTaskListDestinasi().execute("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-7.787461, 110.369270&radius=32000&type=tourist_attraction&key=AIzaSyBbm3-vCnZPRS2R_R1VK8U6oAdJOT6b_3I");
    }

    private class JsonTaskListDestinasi extends AsyncTask< String, String, String > {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    String id = jo.getString("name");
                    String vicinity = jo.getString("vicinity");
                    String rating = jo.getString("rating");

                    Log.d("TAG","vicinity = "+jo.getString("vicinity"));
                    Log.d("TAG","name = "+jo.getString("name"));
                    Log.d("TAG","rating = "+jo.getString("rating"));

                    JSONObject jsonObject1 = jo.getJSONObject("geometry");
                    JSONObject object = jsonObject1.getJSONObject("location");
                    double lat = object.getDouble("lat");
                    double lng = object.getDouble("lng");
                    Log.d("TAG","lat = "+lat+"\n"+"lng = "+lng);

                    JSONArray jsonArray1 = jo.getJSONArray("photos");
                    for (int j=0; j<jsonArray1.length();j++){
                        JSONObject jsonArrayJSONObject = jsonArray1.getJSONObject(j);
                        String  photo_reference = jsonArrayJSONObject.getString("photo_reference");

                        Log.d("TAG","photo_reference= "+photo_reference);
                        destinasiModelList.add(new DestinasiModel(id, vicinity, rating, lat, lng, photo_reference));
                    }
                }

                destinasiAdapter = new DestinasiAdapter(destinasiModelList, DestinasiWisata.this);
                activityDestinasiWisataBinding.rvDestinasiWisata.setLayoutManager(new LinearLayoutManager(DestinasiWisata.this));
                activityDestinasiWisataBinding.rvDestinasiWisata.setAdapter(destinasiAdapter);

                progressDialog.dismiss();
            } catch (Exception e) {
                Log.d("TAG", "Hasil " + e.getMessage());
            }
        }
    }
}